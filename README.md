# Lightynode a WDM and OTN device simulator

Lightynode is a simulator for WDM and OTN devices managed by the Netconf protocol.
It is based on the lighty.io NETCONF Device Simulator (& Libraries) project, an open-source generic Netconf broker
that provides a Netconf Device Library for building custom NETCONF devices.
https://github.com/PANTHEONtech/lighty-netconf-simulator

Lightynode can be considered as a specialized agent of lighty.io NETCONF Device Simulator.
It currently supports OpenROADM device versions 2.2.1. Future versions will support OpenROADM versions 1.2.1 and 7.1 and additionally a support of OPENCONFIG.

* http://openroadm.org/
* http://openconfig.net/

## Installation
* Build the project with Java 17:
```
mvn clean install
```
* The Lighynode simulator build is located at:

`lighty-openroadm-device/target/lighty-openroadm-device-17.1.0.1-SNAPSHOT.jar`

## Usage
`java -jar lighty-openroadm-device/target/lighty-openroadm-device-17.1.0.1-SNAPSHOT.jar -p <port> -f <initial config xml file>`

Eg : `java -jar lighty-openroadm-device/target/lighty-openroadm-device-17.1.0.1-SNAPSHOT.jar -p 17841 -f tests/sample_configs/openroadm/2.2.1/oper-ROADMA.xml`