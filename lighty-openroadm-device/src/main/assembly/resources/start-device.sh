#!/bin/bash

# The script accepts one parameter signifying the listening port of the NETCONF server.
#
# ./start-device -v <openROADM-version> -p <NETCONF-port> -f <device-configuration-file>
#
# When run without a parameter a default port 17830 will be used.

CLASSPATH="$(dirname $0)/lighty-openroadm-device-@project.version@.jar"

for jar in $(ls -1 $(dirname $0)/lib/);
do
   CLASSPATH=$CLASSPATH:lib/$jar
done

java -Xverify:none -server -Xms512m -Xmn512m -Xmx1024m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=512m -classpath "$CLASSPATH" io.lighty.transportpce.netconf.device.openroadm.Main "$@"
