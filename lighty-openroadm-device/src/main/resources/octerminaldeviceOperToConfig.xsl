﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:d="urn:ietf:params:xml:ns:netconf:base:1.0"
xmlns:n="urn:ietf:params:xml:ns:netmod:notification"
xmlns:ood="http://org/openroadm/device"
xmlns:ocp="http://openconfig.net/yang/platform"
xmlns:octd="http://openconfig.net/yang/terminal-device">

  <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
  
  
  <xsl:template match="/*[local-name()='components' or local-name()='terminal-device']">
    <xsl:for-each select="*">
      <xsl:apply-templates select="." />
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="*">
    <xsl:if test="local-name(.) != 'state'">
    <xsl:copy>
      <xsl:for-each select="@*"><xsl:copy-of select="."></xsl:copy-of></xsl:for-each>
        <xsl:choose>
          <xsl:when test="name(.) =  'state' or name(.) = 'operational-modes'"></xsl:when>
          <xsl:otherwise><xsl:apply-templates select="node()" /></xsl:otherwise>
        </xsl:choose>
    </xsl:copy>
    </xsl:if>
  </xsl:template>

  <xsl:template match="//*[local-name() = 'netconf' or local-name()='system' or local-name()='netconf-state' or local-name()='datastores' or local-name()='schemas']" />

  </xsl:stylesheet>
