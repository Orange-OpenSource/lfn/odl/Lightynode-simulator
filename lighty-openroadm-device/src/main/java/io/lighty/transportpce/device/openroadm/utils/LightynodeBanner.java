/*
 * Copyright (c) 2020 PANTHEON.tech s.r.o. All Rights Reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-v10.html
 */
package io.lighty.transportpce.device.openroadm.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LightynodeBanner {
    private static final String[] BANNER = {
            ".__  .__       .__     __              .__          ",
            "|  | |__| ____ |  |___/  |_ ___.__.    |__| ____    ",
            "|  | |  |/ ___\\|  |  \\   __<   |  |    |  |/  _ \\",
            "|  |_|  / /_/  >   Y  \\  |  \\___  |    |  (  <_> )",
            "|____/__\\___  /|___|  /__|  / ____| /\\ |__|\\____/",
            "/_____/     \\/      \\/      \\/           ",
            "Starting lighty.io Lightynode-simulator application ...",
            "https://gitlab.com/Orange-OpenSource/lfn/odl/Lightynode-simulator",
            "https://lighty.io/",
            "https://github.com/PantheonTechnologies/lighty-core" };

        private static final Logger LOG = LoggerFactory.getLogger(LightynodeBanner.class);

        /**
         * Private constructor.
         */
        private LightynodeBanner() {

        }

        public static void print() {
            for (String line : BANNER) {
                LOG.info(line);
            }
//            LOG.info(":: Version :: {}", getVersion());
        }

        private static String getVersion() {
            Package lightyPackage = LightynodeBanner.class.getPackage();
            if (lightyPackage != null && lightyPackage.getImplementationVersion() != null) {
                return lightyPackage.getImplementationVersion();
            }
            return "not defined";
        }

}