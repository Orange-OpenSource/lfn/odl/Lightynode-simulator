/*
 * Copyright (c) 2020 PANTHEON.tech s.r.o. All Rights Reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-v10.html
 */

package io.lighty.transportpce.device.openroadm.utils;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.opendaylight.yangtools.yang.binding.YangModuleInfo;
import com.google.common.collect.ImmutableSet;

import io.lighty.modules.northbound.restconf.community.impl.util.RestConfConfigUtils;
import io.lighty.modules.southbound.netconf.impl.util.NetconfConfigUtils;

public final class ModelsUtils {

    public static final Set<YangModuleInfo> OPENROADM_MODEL_PATHS_121 = ImmutableSet.of(
    org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.netmod.notification.rev080714.YangModuleInfoImpl
        .getInstance(),
    org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.netconf.base._1._0.rev110601.YangModuleInfoImpl
        .getInstance(),
    org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.yang.ietf.netconf.monitoring.rev101004.YangModuleInfoImpl
        .getInstance(),
    org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.netconf.notification._1._0.rev080714.YangModuleInfoImpl
        .getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.alarm.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.common.types.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.equipment.states.types.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.maintenance.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.otn.common.types.rev171215.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.pm.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.pm.types.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.port.types.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.probablecause.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.resource.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.resource.types.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.tca.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.user.mgmt.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.database.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.de.operations.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.de.swdl.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.device.rev170206.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.ethernet.interfaces.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.file.transfer.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.fwdl.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.interfaces.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.lldp.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.maintenance.loopback.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.maintenance.testsignal.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.optical.channel.interfaces.rev161014.YangModuleInfoImpl
        .getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.optical.transport.interfaces.rev161014.YangModuleInfoImpl
        .getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.otn.common.rev170626.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.otn.odu.interfaces.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.otn.otu.interfaces.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.physical.types.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.rstp.rev161014.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.syslog.rev171215.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.org.openroadm.wavelength.map.rev171215.YangModuleInfoImpl.getInstance(),
    org.opendaylight.yang.svc.v1.http.honeynode.simulator.pm.handling.rev230711.YangModuleInfoImpl.getInstance());

    private static final Set<YangModuleInfo> MODEL_PATH_121 = Stream.concat(
        Stream.concat(
                RestConfConfigUtils.YANG_MODELS.stream(),
                NetconfConfigUtils.NETCONF_TOPOLOGY_MODELS.stream())
        .collect(Collectors.toSet()).stream(),
        OPENROADM_MODEL_PATHS_121.stream()).collect(Collectors.toSet());

    public static Set<YangModuleInfo> getYangModels121() {
        return MODEL_PATH_121;
    }

    public static final Set<YangModuleInfo> OPENROADM_MODEL_PATHS_221 = ImmutableSet.of(
        org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.netmod.notification.rev080714.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.netconf.base._1._0.rev110601.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.yang.ietf.netconf.monitoring.rev101004.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.netconf.notification._1._0.rev080714.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.alarm.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.common.types.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.equipment.states.types.rev171215.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.maintenance.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.otn.common.types.rev171215.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.pm.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.pm.types.rev171215.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.port.types.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.probablecause.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.resource.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.resource.types.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.switching.pool.types.rev171215.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.tca.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.user.mgmt.rev171215.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.database.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.de.operations.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.de.swdl.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.device.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.ethernet.interfaces.rev181019.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.file.transfer.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.fwdl.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.interfaces.rev170626.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.lldp.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.maintenance.loopback.rev171215.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.maintenance.testsignal.rev171215.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.media.channel.interfaces.rev181019.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.network.media.channel.interfaces.rev181019.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.optical.channel.interfaces.rev181019.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.optical.transport.interfaces.rev181019.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.otn.common.rev170626.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.otn.odu.interfaces.rev181019.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.otn.otu.interfaces.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.physical.types.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.pluggable.optics.holder.capability.rev181019.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.port.capability.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.prot.otn.linear.aps.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.rstp.rev181019.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.syslog.rev171215.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.wavelength.map.rev171215.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.honeynode.simulator.pm.handling.rev211021.YangModuleInfoImpl.getInstance());

    private static final Set<YangModuleInfo> MODEL_PATH_221 = Stream.concat(
        Stream.concat(
                RestConfConfigUtils.YANG_MODELS.stream(),
                NetconfConfigUtils.NETCONF_TOPOLOGY_MODELS.stream())
        .collect(Collectors.toSet()).stream(),
        OPENROADM_MODEL_PATHS_221.stream()).collect(Collectors.toSet());

    public static Set<YangModuleInfo> getYangModels221() {
        return MODEL_PATH_221;
    }

    public static final Set<YangModuleInfo> OPENROADM_MODEL_PATHS_71 = ImmutableSet.of(
        org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.netmod.notification.rev080714.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.netconf.base._1._0.rev110601.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.yang.ietf.netconf.monitoring.rev101004.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.netconf.notification._1._0.rev080714.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.honeynode.simulator.pm.handling.rev230629.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.alarm.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.common.alarm.pm.types.rev191129.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.common.amplifier.types.rev191129.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.common.attributes.rev200327.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.common.equipment.types.rev191129.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.common.link.types.rev191129.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.common.node.types.rev191129.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.common.optical.channel.types.rev200529.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.common.state.types.rev191129.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.common.types.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.equipment.states.types.rev191129.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.interfaces.rev191129.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.layerrate.rev191129.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.manifest.file.rev200327.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.network.resource.rev191129.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.otn.common.types.rev200327.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.pm.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.pm.types.rev200327.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.port.types.rev200327.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.probablecause.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.resource.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.resource.types.rev191129.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.service.format.rev191129.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.switching.pool.types.rev191129.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.tca.rev200327.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.user.mgmt.rev191129.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.database.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.de.operations.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.device.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.device.types.rev191129.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.dhcp.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.ethernet.interfaces.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.file.transfer.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.fwdl.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.gcc.interfaces.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.gnmi.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.ip.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.ipv4.unicast.routing.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.ipv6.unicast.routing.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.key.chain.rev191129.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.lldp.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.maintenance.loopback.rev191129.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.maintenance.testsignal.rev200529.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.media.channel.interfaces.rev200529.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.network.media.channel.interfaces.rev200529.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.optical.channel.interfaces.rev200529.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.optical.operational.interfaces.rev200529.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.optical.transport.interfaces.rev200529.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.optical.channel.tributary.signal.interfaces.rev200529
            .YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.otn.common.rev200327.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.otn.odu.interfaces.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.otn.otu.interfaces.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.otsi.group.interfaces.rev200529.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.physical.types.rev191129.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.pluggable.optics.holder.capability.rev200529.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.port.capability.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.ppp.interfaces.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.prot.otn.linear.aps.rev200529.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.routing.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.rstp.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.security.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.de.swdl.rev200529.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.syslog.rev191129.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.telemetry.types.rev191129.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.org.openroadm.wavelength.map.rev191129.YangModuleInfoImpl.getInstance());

    private static final Set<YangModuleInfo> MODEL_PATH_71 = Stream.concat(
        Stream.concat(
                RestConfConfigUtils.YANG_MODELS.stream(),
                NetconfConfigUtils.NETCONF_TOPOLOGY_MODELS.stream())
        .collect(Collectors.toSet()).stream(),
        OPENROADM_MODEL_PATHS_71.stream()).collect(Collectors.toSet());

    public static Set<YangModuleInfo> getYangModels71() {
        return MODEL_PATH_71;
    }

    public static final Set<YangModuleInfo> OPENCONFIG_MODEL_PATHS_240119 = ImmutableSet.of(
        org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.netmod.notification.rev080714.YangModuleInfoImpl
        .getInstance(),
        org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.netconf.notification._1._0.rev080714.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.yang.ietf.restconf.rev170126.YangModuleInfoImpl
        .getInstance(),
        org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.yang.ietf.yang.library.rev190104.YangModuleInfoImpl
        .getInstance(),
        org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.yang.ietf.interfaces.rev180220.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.yang.iana._if.type.rev230126.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.urn.opendaylight.netconf.keystore.rev231109.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.terminal.device.rev210729.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.platform.rev220610.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.platform.port.rev211001.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.transport.line.common.rev190603.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.platform.linecard.rev220421.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.openconfig.ext.rev200616.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.openconfig.types.rev190416.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.platform.types.rev220327.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.types.yang.rev210714.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.platform.psu.rev181121.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.platform.fan.rev181121.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.alarms.types.rev181121.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.alarms.rev190709.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.system.rev210720.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.interfaces.rev210406.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.urn.ietf.params.xml.ns.yang.ietf.interfaces.rev180220.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.openconfig.transport.line.connectivity.rev190627
            .YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.platform.transceiver.rev210729.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.transport.types.rev210729.YangModuleInfoImpl
            .getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.platform.cpu.rev181121.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.aaa.types.rev181121.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.aaa.rev200730.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.telemetry.types.rev181121.YangModuleInfoImpl.getInstance(),
        org.opendaylight.yang.svc.v1.http.openconfig.net.yang.telemetry.rev181121.YangModuleInfoImpl.getInstance()
        );

    private static final Set<YangModuleInfo> MODEL_PATHS_240119 = Stream.concat(
        Stream.concat(
                RestConfConfigUtils.YANG_MODELS.stream(),
                NetconfConfigUtils.NETCONF_TOPOLOGY_MODELS.stream())
        .collect(Collectors.toSet()).stream(),
        OPENCONFIG_MODEL_PATHS_240119.stream()).collect(Collectors.toSet());

    public static Set<YangModuleInfo> getYangModels240119() {
        return MODEL_PATHS_240119;
    }

    private ModelsUtils() {
        throw new UnsupportedOperationException("Please do not instantiate utility class.");
    }
}
