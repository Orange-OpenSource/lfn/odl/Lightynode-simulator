/*
 * Copyright (c) 2020 PANTHEON.tech s.r.o. All Rights Reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-v10.html
 */

package io.lighty.transportpce.netconf.device.openroadm;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

import org.opendaylight.yangtools.yang.binding.YangModuleInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lighty.core.controller.api.AbstractLightyModule;
import io.lighty.transportpce.netconf.device.NetconfDevice;
import io.lighty.transportpce.netconf.device.NetconfDeviceBuilder;

public class TerminalDevice240119 extends AbstractLightyModule implements TerminalDevice {
    private static final Logger LOG = LoggerFactory.getLogger(TerminalDevice240119.class);
//    private DataTreeChangeListenerActivatorFactory221 listenerActivator;

    public TerminalDevice240119 (Set<YangModuleInfo> models, int initPort, boolean initDataStore,
            InputStream initialOperationalData, InputStream initialConfigData) throws IOException {
        LOG.info("creating TerminalDevice-240119");

        final NetconfDeviceBuilder netconfDeviceBuilder = new NetconfDeviceBuilder()
            .setCredentials("admin", "admin")
            .setBindingPort(initPort)
            .withModels(models)
            .withDefaultRequestProcessors()
            .withDefaultCapabilities()
            .withDefaultNotificationProcessor()
            .withNetconfMonitoringEnabled(true);

        if (initDataStore) {
            String initialConfigurationData = OcConfiguration.loadConfigData(initialConfigData);
            netconfDeviceBuilder.setInitialOperationalData(initialOperationalData)
                    .setInitialConfigurationData(new ByteArrayInputStream(initialConfigurationData.getBytes()));

        }
        // Initialize NetconfDevice and start it from netConfDeviceSimulator
        NetconfDevice netconfDevice = netconfDeviceBuilder.build();
        netconfDevice.start();

    }

    @Override
    protected boolean initProcedure() throws InterruptedException {
        LOG.info("Initializing TerminalDevice-240119");
        return true;
    }

    @Override
    protected boolean stopProcedure() throws InterruptedException {
        LOG.info("Stoping TerminalDevice-240119");
        return true;
    }

}
