package io.lighty.transportpce.netconf.device.openroadm;

import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

import org.opendaylight.mdsal.binding.api.DataBroker;
import org.opendaylight.mdsal.binding.api.ReadTransaction;
import org.opendaylight.mdsal.binding.api.WriteTransaction;
import org.opendaylight.mdsal.common.api.CommitInfo;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.ChangeNotification;
import org.opendaylight.yangtools.yang.binding.DataObject;
import org.opendaylight.yangtools.yang.binding.EnumTypeObject;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.opendaylight.yangtools.yang.binding.Notification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import io.lighty.transportpce.netconf.device.requests.notification.NotificationPublishService;
import javassist.expr.Instanceof;

public final class DeviceUtils {
    private static final Logger LOG = LoggerFactory.getLogger(DeviceUtils.class);

    public static final String DEVICE_DATA_SAMPLE_OPER_XML = Main.getOperFile();
    public static final String DEVICE_XSL = "/config.xsl";
    public static final String CONFIG_XSL = "/OperToConfig.xsl";
    public static final String CONFIG_XSL_121 = "/OperToConfig121.xsl";
    public static final String OC_CONFIG_XSL = "/octerminaldeviceOperToConfig.xsl";
    private DataBroker dataBroker;

    private final ListeningExecutorService executor;
    private NotificationPublishService notificationPublishService;

    public DeviceUtils(DataBroker dataBroker, NotificationPublishService notificationPublishService) {
        this.dataBroker = dataBroker;
        this.notificationPublishService = notificationPublishService;
        this.executor = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(5));
    }

    public DataBroker getDataBroker() {
        return dataBroker;
    }

    /**
     * update the operational state of any device container (port, circuit-pack,
     * etc) according to the administrative-state configured on it.
     *
     * @param adminState
     *            AdminStates configured on the container
     * @return operational-state of the container for operational datastore purpose
     */
    public final <T> T setOperationalStateFromAdminState(EnumTypeObject adminState) {
        if (adminState instanceof
            org.opendaylight.yang.gen.v1.http.org.openroadm.equipment.states.types.rev161014.AdminStates) {
        return switch (adminState.getName()) {
            case "inService" -> (T) org.opendaylight.yang.gen.v1.http.org.openroadm.common.types.rev161014.State.InService;
            case "maintenance" -> (T) org.opendaylight.yang.gen.v1.http.org.openroadm.common.types.rev161014.State.Degraded;
            case "outOfService" -> (T) org.opendaylight.yang.gen.v1.http.org.openroadm.common.types.rev161014.State.OutOfService;
            default -> null;
        };
    }
        if (adminState instanceof
                org.opendaylight.yang.gen.v1.http.org.openroadm.equipment.states.types.rev171215.AdminStates) {
            return switch (adminState.getName()) {
                case "inService" -> (T) org.opendaylight.yang.gen.v1.http.org.openroadm.common.types.rev181019.State.InService;
                case "maintenance" -> (T) org.opendaylight.yang.gen.v1.http.org.openroadm.common.types.rev181019.State.Degraded;
                case "outOfService" -> (T) org.opendaylight.yang.gen.v1.http.org.openroadm.common.types.rev181019.State.OutOfService;
                default -> null;
            };
        }
        if (adminState instanceof
                org.opendaylight.yang.gen.v1.http.org.openroadm.equipment.states.types.rev191129.AdminStates) {
            return switch (adminState.getName()) {
                case "inService" -> (T) org.opendaylight.yang.gen.v1.http.org.openroadm.common.state.types.rev191129.State.InService;
                case "maintenance" -> (T) org.opendaylight.yang.gen.v1.http.org.openroadm.common.state.types.rev191129.State.Degraded;
                case "outOfService" -> (T) org.opendaylight.yang.gen.v1.http.org.openroadm.common.state.types.rev191129.State.OutOfService;
                default -> null;
        };
    }
        LOG.error("Device version of admin state not managed yet");
        return null;
    }

    private <T,  N extends Notification<N> & DataObject> T getQnameNotifVersion(N notif) {
        if (notif instanceof org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.ChangeNotification) {
            return (T) org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.ChangeNotification.QNAME;
        }
        if (notif instanceof org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.ChangeNotification) {
            return (T) org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.ChangeNotification.QNAME;
        }
        if (notif instanceof org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.ChangeNotification) {
            return (T) org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.ChangeNotification.QNAME;
        }
        return null;
    }
    /**
     * get container from OrgOpenroadmDevice datastore.
     *
     * @param <T> the generic type
     * @param datastore LogicalDatastoreType
     * @param path Instance Identifier of the container
     * @return container or null if read transaction issue
     */

    public <T extends DataObject> T readContainerFromDataStore(LogicalDatastoreType datastore,
        InstanceIdentifier<T> path) {
        LOG.info("reading {} from {} datastore ...", path.getTargetType().getSimpleName(), datastore);

        try(ReadTransaction readTransaction = this.getDataBroker().newReadOnlyTransaction()) {
            Optional<T> optional = readTransaction.read(datastore, path)
                .get();
            if (optional.isPresent()) {
                return optional.get();
            }
        } catch (InterruptedException e) {
            LOG.error("Failed to process DeleteTransaction", e);
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            LOG.error("Failed to process DeleteTransaction", e);
        }
        return null;
    }

    /**
     * Write container into data store.
     *
     * @param <T> the generic type
     * @param datastore LogicalDatastoreType
     * @param path Instance Identifier of the container
     * @param container to write/update into the device datastore
     */
    public <T extends DataObject, N extends Notification<N> & DataObject> void writeContainerIntoDataStore(
            LogicalDatastoreType datastore, InstanceIdentifier<T> path, T container, N notif) {
        LOG.info("writing container {} into {} datastore ...", path.getTargetType().getSimpleName(), datastore);
        WriteTransaction writeTransaction = this.getDataBroker().newWriteOnlyTransaction();
        writeTransaction.merge(datastore, path, container);
        Futures.addCallback(writeTransaction.commit(), new FutureCallback<CommitInfo>() {
            public void onSuccess(CommitInfo result) {
                LOG.info("commit successfully done : {}", result);
                if (notif != null && notificationPublishService != null) {
                    LOG.info("sending notification from DevicePortChangeListener");
                    notificationPublishService.publish(notif, getQnameNotifVersion(notif));
                }
            }
            public void onFailure(Throwable t) {
                LOG.error("commit failed due to another type of TransactionCommitFailedException : {}", t);
            }
        }, executor);
    }

    /**
     * Delete container from data store.
     *
     * @param <T> the generic type
     * @param datastore LogicalDatastoreType
     * @param path Instance Identifier of the container
     */
    public <T extends DataObject> void deleteContainerFromDataStore(LogicalDatastoreType datastore,
        InstanceIdentifier<T> path) {
    LOG.info("deleting container {} from {} datastore ...", path.getTargetType().getSimpleName(), datastore);
    WriteTransaction writeTransaction = this.getDataBroker().newWriteOnlyTransaction();
    writeTransaction.delete(datastore, path);
    Futures.addCallback(writeTransaction.commit(), new FutureCallback<CommitInfo>() {
        public void onSuccess(CommitInfo result) {
            LOG.info("commit successfully done : {}", result);
        }

        public void onFailure(Throwable t) {
            LOG.error("commit failed due to another type of TransactionCommitFailedException : {}", t);
        }
    }, executor);
}

    /**
     * Overwrite container into data store.
     *
     * @param <T> the generic type
     * @param datastore LogicalDatastoreType
     * @param path the path
     * @param container Instance Identifier of the container
     */
    public <T extends DataObject> void overwriteContainerIntoDataStore(LogicalDatastoreType datastore,
        InstanceIdentifier<T> path, T container) {
        LOG.info("writing container {} into {} datastore ...", path.getTargetType().getSimpleName(), datastore);
        WriteTransaction writeTransaction = this.getDataBroker().newWriteOnlyTransaction();
        writeTransaction.merge(datastore, path, container);
        Futures.addCallback(writeTransaction.commit(), new FutureCallback<CommitInfo>() {
            public void onSuccess(CommitInfo result) {
                LOG.info("commit successfully done : {}", result);
            }

            public void onFailure(Throwable t) {
                LOG.error("commit failed due to another type of TransactionCommitFailedException : {}", t);
            }
        }, executor);
    }
}
