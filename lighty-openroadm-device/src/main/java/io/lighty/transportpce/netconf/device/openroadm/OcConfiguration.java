package io.lighty.transportpce.netconf.device.openroadm;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public final class OcConfiguration {
    private static final Logger LOG = LoggerFactory.getLogger(OcConfiguration.class);

    public OcConfiguration() {
    }

    public static String loadConfigData(InputStream operInputStream) throws IOException {
        LOG.info("loading device configuration info from xml file...");
        String config_result = null;
        Path tempOperInputFile = Files.createTempDirectory("").resolve(UUID.randomUUID().toString() + ".tmp");
        Files.copy(operInputStream, tempOperInputFile, StandardCopyOption.REPLACE_EXISTING);
        String xml = new String(Files.readAllBytes(tempOperInputFile));
        if (xml != null) {
            LOG.info("process to transform xml file to config data");
            TransformerFactory factory = TransformerFactory.newInstance();
            Source xslt = new StreamSource(OcConfiguration.class.getResourceAsStream(DeviceUtils.OC_CONFIG_XSL));
            Transformer transformer;
            Source text;
            StringWriter device_config = new StringWriter();
            try {
                LOG.info("transforming xml string to config device ...");
                transformer = factory.newTransformer(xslt);
                text = new StreamSource(new StringReader(xml));
                transformer.transform(text, new StreamResult(device_config));
                config_result = device_config.toString();
            } catch (TransformerException e) {
                LOG.error("Transformer failed ");
            }
        }
        return config_result;
    }
}
