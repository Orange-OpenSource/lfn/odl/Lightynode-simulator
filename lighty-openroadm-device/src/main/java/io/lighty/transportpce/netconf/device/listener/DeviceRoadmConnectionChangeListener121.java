package io.lighty.transportpce.netconf.device.listener;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.opendaylight.mdsal.binding.api.DataObjectModification;
import org.opendaylight.mdsal.binding.api.DataTreeChangeListener;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.mdsal.binding.api.DataTreeModification;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.org.openroadm.device.container.org.openroadm.device.RoadmConnections;
import org.opendaylight.yangtools.yang.binding.DataObjectStep;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lighty.transportpce.netconf.device.openroadm.DeviceUtils;

/**
 * The listener interface for receiving deviceRoadmConnectionChange events.
 * The class that is interested in processing a deviceRoadmConnectionChange
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's addDeviceRoadmConnectionChangeListener method. When
 * the deviceRoadmConnectionChange event occurs, that object's appropriate
 * method is invoked.
 */
public class DeviceRoadmConnectionChangeListener121 implements DataTreeChangeListener<RoadmConnections> {
    private static final Logger LOG = LoggerFactory.getLogger(DeviceRoadmConnectionChangeListener121.class);
    private DeviceUtils deviceUtils;

    public DeviceRoadmConnectionChangeListener121(DeviceUtils deviceUtils) {
        this.deviceUtils = deviceUtils;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onDataTreeChanged(@NonNull List<DataTreeModification<RoadmConnections>> changes) {
        LOG.info("onDataTreeChanged - RoadmConnections");
        for (DataTreeModification<RoadmConnections> change : changes) {
            final DataObjectModification<RoadmConnections> rootNode = change.getRootNode();
            final DataTreeIdentifier<RoadmConnections> rootPath = change.getRootPath();
            LOG.debug("Received RoadmConnections change({}):\n dataType={} \n before={} \n after={} \n identifier = {}",
                rootNode.modificationType(), rootNode.dataType(), rootNode.dataBefore(),
                rootNode.dataAfter(), rootNode.step());
            InstanceIdentifier<RoadmConnections> connectionId = null;
            if (rootPath.path().getTargetType().getName().equals(RoadmConnections.class.getName())) {
                LinkedList<DataObjectStep<?>> path = new LinkedList<>();
                rootPath.path().getPathArguments().forEach(p -> path.add(p));
                connectionId = InstanceIdentifier.unsafeOf(path);
            }
            switch (rootNode.modificationType()) {
            case SUBTREE_MODIFIED:
            case WRITE:
                LOG.info("RoadmConnections is being created or modified");
                copyConnectionOnOperationalDS(rootNode.dataAfter(), connectionId);
                break;
            case DELETE:
                LOG.info("RoadmConnections is being deleted");
                deleteConnectionOnOperationalDS(LogicalDatastoreType.OPERATIONAL, connectionId);
                break;
            default:
                break;
            }
        }
    }

    private void deleteConnectionOnOperationalDS(LogicalDatastoreType datastore,
        InstanceIdentifier<RoadmConnections> connectionId) {
        if (connectionId == null) {
            LOG.warn("Error to copy connection in operational DS - connection instance id is null");
            return;
        }
        LOG.info("deleting container {} from {} datastore ...", connectionId.getTargetType().getSimpleName(),
            datastore);
        deviceUtils.deleteContainerFromDataStore(datastore, connectionId);
    }

    /**
     * copy the connection in the operational datastore after a change received on
     * the config datastore.
     *
     * @param connectionAfter
     *            new configuration of the circuit-pack
     * @param id
     *            Instance Identifier of the circuit-pack
     */
    private void copyConnectionOnOperationalDS(RoadmConnections connectionAfter,
        InstanceIdentifier<RoadmConnections> connectionId) {
        if (connectionId == null) {
            LOG.warn("Error to copy connection in operational DS - connection instance id is null");
            return;
        }
        RoadmConnections connection = deviceUtils.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL,
            connectionId);
            if (connection != null) {
                LOG.info("Updating existing connection {} in operational DS", connectionAfter.getConnectionNumber());
                deviceUtils.overwriteContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, connectionId,
                    connectionAfter);
            } else {
                LOG.info("creating new connection {} in operational DS", connectionAfter.getConnectionNumber());
                deviceUtils.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, connectionId,
                    connectionAfter, null);
            }
    }
}
