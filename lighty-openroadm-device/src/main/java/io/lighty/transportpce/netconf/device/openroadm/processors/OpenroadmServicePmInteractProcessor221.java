/*
 * Copyright (c) 2020 PANTHEON.tech s.r.o. All Rights Reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-v10.html
 */
package io.lighty.transportpce.netconf.device.openroadm.processors;

import java.util.concurrent.Future;

import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021.PmInteract;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021.PmInteractInput;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021.PmInteractOutput;
import org.opendaylight.yangtools.yang.common.QName;
import org.opendaylight.yangtools.yang.common.RpcResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.ListenableFuture;

import io.lighty.transportpce.netconf.device.openroadm.rpcs.OrgOpenroadmDeviceServiceImpl221;

@SuppressWarnings("checkstyle:MemberName")
public class OpenroadmServicePmInteractProcessor221 extends OpenroadmServiceAbstractProcessor
        <PmInteractInput, PmInteractOutput> implements PmInteract{

    private static final Logger LOG = LoggerFactory.getLogger(OpenroadmServicePmInteractProcessor221.class);

    private final OrgOpenroadmDeviceServiceImpl221 orgOpenroadmDeviceService;
    private final QName qName = QName.create("http://honeynode-simulator/pm-handling", "pm-interact");

    public OpenroadmServicePmInteractProcessor221(OrgOpenroadmDeviceServiceImpl221 openroadmDeviceService) {
        this.orgOpenroadmDeviceService = openroadmDeviceService;
    }

    @Override
    public QName getIdentifier() {
        return this.qName;
    }

    @Override
    protected Future<RpcResult<PmInteractOutput>> execMethod(PmInteractInput input) {
        LOG.info("execute RPC: PmInteract");
        return orgOpenroadmDeviceService.pmInteract(input);
    }

    @Override
    public ListenableFuture<RpcResult<PmInteractOutput>> invoke(PmInteractInput input) {
        // TODO Auto-generated method stub
        return null;
    }
}
