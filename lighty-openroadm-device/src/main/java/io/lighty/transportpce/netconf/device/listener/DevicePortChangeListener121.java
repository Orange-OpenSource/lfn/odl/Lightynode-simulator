package io.lighty.transportpce.netconf.device.listener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.eclipse.jdt.annotation.NonNull;
import org.opendaylight.mdsal.binding.api.DataObjectModification;
import org.opendaylight.mdsal.binding.api.DataTreeChangeListener;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.mdsal.binding.api.DataTreeModification;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.ChangeNotification;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.ChangeNotification.Datastore;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.ChangeNotificationBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.change.notification.Edit;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.change.notification.EditBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.changed.by.parms.ChangedBy;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.changed.by.parms.ChangedByBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.changed.by.parms.changed.by.server.or.user.ServerBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.circuit.pack.Ports;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.circuit.pack.PortsBuilder;
import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.netconf.base._1._0.rev110601.EditOperationType;
import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.yang.ietf.yang.types.rev130715.DateAndTime;
import org.opendaylight.yangtools.yang.binding.DataObjectStep;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.opendaylight.yangtools.yang.common.Empty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lighty.transportpce.netconf.device.openroadm.DeviceUtils;
import io.lighty.transportpce.netconf.device.requests.notification.NotificationPublishService;

public class DevicePortChangeListener121 implements DataTreeChangeListener<Ports> {
    private static final Logger LOG = LoggerFactory.getLogger(DevicePortChangeListener121.class);
    private DeviceUtils deviceUtils;
    private NotificationPublishService notificationPublishService;

    public DevicePortChangeListener121(DeviceUtils deviceUtils) {
        this.deviceUtils = deviceUtils;
    }

    public void setNotificationPublishService(NotificationPublishService notificationPublishService) {
        this.notificationPublishService = notificationPublishService;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onDataTreeChanged(@NonNull List<DataTreeModification<Ports>> changes) {
        LOG.info("onDataTreeChanged - Ports");
        for (DataTreeModification<Ports> change : changes) {
            final DataObjectModification<Ports> rootNode = change.getRootNode();
            final DataTreeIdentifier<Ports> rootPath = change.getRootPath();
            if (rootNode != null && rootNode.dataBefore() != null
                && !rootNode.dataBefore().equals(rootNode.dataAfter())) {
                LOG.info("Received Ports change({}):\n dataType={} \n before={} \n after={} \n identifier = {}",
                    rootNode.modificationType(), rootNode.dataType(), rootNode.dataBefore(),
                    rootNode.dataAfter(), rootNode.step());
                InstanceIdentifier<Ports> portId = null;
                if (rootPath.path().getTargetType().getName().equals(Ports.class.getName())) {
                    LinkedList<DataObjectStep<?>> path = new LinkedList<>();
                    rootPath.path().getPathArguments().forEach(p -> path.add(p));
                    portId = InstanceIdentifier.unsafeOf(path);
                    LOG.info("portId = {}", portId.toString());
                }
                switch (rootNode.modificationType()) {
                case SUBTREE_MODIFIED:
                case WRITE:
                    LOG.info("port is being modified");
                    updatePortOnOperationalDS(rootNode.dataBefore(), rootNode.dataAfter(), portId);
                    break;
                case DELETE:
                    LOG.info("port is being deleted");
                    break;
                default:
                    break;
                }
            }
        }
    }

    private synchronized void updatePortOnOperationalDS(Ports portBefore, Ports portAfter,
        InstanceIdentifier<Ports> portId) {
        if (portId == null) {
            LOG.warn("Error to update port in operational DS - port instance id is null");
            return;
        }
        Ports oldPort = deviceUtils.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, portId);
        PortsBuilder portBldr = new PortsBuilder(oldPort);
        if (!portBefore.getAdministrativeState().equals(portAfter.getAdministrativeState())) {
            LOG.info("administrative state of port {} changed from {} to {}", portBefore.getPortName(),
                portBefore.getAdministrativeState(), portAfter.getAdministrativeState());
            portBldr.setAdministrativeState(portAfter.getAdministrativeState())
                .setOperationalState(deviceUtils.setOperationalStateFromAdminState(portAfter.getAdministrativeState()));
            ChangeNotification changeNotification = createChangeNotification(portId);
            deviceUtils.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, portId, portBldr.build(),
                changeNotification);
        }
    }

    private ChangeNotification createChangeNotification(InstanceIdentifier<Ports> id) {
        List<Edit> editList = new ArrayList<>();
        Edit edit = new EditBuilder()
            .setOperation(EditOperationType.Merge)
            .setTarget(id)
            .build();
        editList.add(edit);
        ChangedBy changedBy = new ChangedByBuilder().setServerOrUser(new ServerBuilder().setServer(Empty.value())
            .build())
            .build();
        String time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX").format(new Date());
        return new ChangeNotificationBuilder().setChangeTime(new DateAndTime(time))
            .setDatastore(Datastore.Running).setEdit(editList).build();
    }
}
