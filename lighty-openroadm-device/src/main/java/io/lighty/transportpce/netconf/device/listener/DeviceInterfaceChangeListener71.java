package io.lighty.transportpce.netconf.device.listener;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.eclipse.jdt.annotation.NonNull;
import org.opendaylight.mdsal.binding.api.DataObjectModification;
import org.opendaylight.mdsal.binding.api.DataTreeChangeListener;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.mdsal.binding.api.DataTreeModification;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.OrgOpenroadmDeviceData;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.pack.Ports;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.pack.PortsBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.pack.PortsKey;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.packs.CircuitPacks;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.packs.CircuitPacksKey;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.interfaces.grp.Interface;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.interfaces.grp.InterfaceBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.org.openroadm.device.container.OrgOpenroadmDevice;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.port.Interfaces;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.port.InterfacesBuilder;
import org.opendaylight.yangtools.yang.binding.DataObjectStep;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lighty.transportpce.netconf.device.openroadm.DeviceUtils;

/**
 * The listener interface for receiving deviceInterfaceChange events.
 * The class that is interested in processing a deviceInterfaceChange
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's addDeviceInterfaceChangeListener method. When
 * the deviceInterfaceChange event occurs, that object's appropriate
 * method is invoked.
 */
public class DeviceInterfaceChangeListener71 implements DataTreeChangeListener<Interface> {
    private static final Logger LOG = LoggerFactory.getLogger(DeviceInterfaceChangeListener71.class);
    private DeviceUtils deviceUtils;

    public DeviceInterfaceChangeListener71(DeviceUtils deviceUtils) {
        this.deviceUtils = deviceUtils;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onDataTreeChanged(@NonNull List<DataTreeModification<Interface>> changes) {

        LOG.info("onDataTreeChanged - Interface");
        changes.stream().forEach(change -> {
            final DataObjectModification<Interface> rootNode = change.getRootNode();
            final DataTreeIdentifier<Interface> rootPath = change.getRootPath();

            if (rootNode != null) {
                LOG.info("Received Interface change({}):\n dataType={} \n before={} \n after={} \n identifier = {}",
                    rootNode.modificationType(), rootNode.dataType(), rootNode.dataBefore(),
                    rootNode.dataAfter(), rootNode.step());
                InstanceIdentifier<Interface> interfaceId = null;
                if (rootPath.path().getTargetType().getName().equals(Interface.class.getName())) {
                    LinkedList<DataObjectStep<?>> path = new LinkedList<>();
                    rootPath.path().getPathArguments().forEach(p -> path.add(p));
                    interfaceId = InstanceIdentifier.unsafeOf(path);
                }
                switch (rootNode.modificationType()) {
                case SUBTREE_MODIFIED:
                case WRITE:
                    LOG.info("Interface is being created or modified");
                    updateInterfaceOnOperationalDS(rootNode.dataBefore(), rootNode.dataAfter(), interfaceId);
                    break;
                case DELETE:
                    LOG.info("Interface is being deleted");
                    deleteContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, interfaceId);
                    updateCpPortInterfaceList(rootNode.dataBefore(), true);
                    break;
                default:
                    break;
                }
            }
        });
    }

    private void deleteContainerFromDataStore(LogicalDatastoreType datastore,
        InstanceIdentifier<Interface> interfaceId) {
        deviceUtils.deleteContainerFromDataStore(datastore, interfaceId);
        LOG.info("deleting container {} into {} datastore ...", interfaceId.getTargetType().getSimpleName(), datastore);
    }

    private void updateInterfaceOnOperationalDS(Interface interfaceBefore, Interface interfaceAfter,
        InstanceIdentifier<Interface> interfaceId) {
        if (interfaceId == null) {
            LOG.warn("Error to create or update interface in operational DS - interface instance id is null");
            return;
        }
        Interface oldInterface = deviceUtils.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, interfaceId);
        InterfaceBuilder interfaceBldr;
        if (oldInterface != null) {
            interfaceBldr = new InterfaceBuilder(oldInterface);
            if ((interfaceBefore != null && interfaceBefore.getAdministrativeState() == null
                && interfaceAfter.getAdministrativeState() != null)
                || (interfaceBefore != null
                    && !interfaceBefore.getAdministrativeState().equals(interfaceAfter.getAdministrativeState()))) {
                LOG.info("administrative state of interface {} changed", interfaceBefore.getName());
                interfaceBldr.setAdministrativeState(interfaceAfter.getAdministrativeState())
                    .setOperationalState(
                        deviceUtils.setOperationalStateFromAdminState(interfaceAfter.getAdministrativeState()));
                deviceUtils.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, interfaceId,
                    interfaceBldr.build(), null);
                LOG.info("writing container {} into {} datastore ...", interfaceId.getTargetType().getSimpleName(),
                    LogicalDatastoreType.OPERATIONAL);
            }
        } else {
            LOG.info("creating new interface {} in operational DS", interfaceAfter.getName());
            interfaceBldr = new InterfaceBuilder(interfaceAfter)
                .setOperationalState(
                    deviceUtils.setOperationalStateFromAdminState(interfaceAfter.getAdministrativeState()));
            deviceUtils.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, interfaceId,
                interfaceBldr.build(), null);
            LOG.info("writing container {} into {} datastore ...", interfaceId.getTargetType().getSimpleName(),
                LogicalDatastoreType.OPERATIONAL);
            updateCpPortInterfaceList(interfaceAfter, false);
        }
    }

    private void updateCpPortInterfaceList(Interface interf, boolean isDeleting) {
        InstanceIdentifier<Ports> supportedPortId = InstanceIdentifier
            .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class)
            .child(CircuitPacks.class, new CircuitPacksKey(interf.getSupportingCircuitPackName()))
            .child(Ports.class, new PortsKey(interf.getSupportingPort().toString())).build();
        Ports supportedPort = deviceUtils.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, supportedPortId);
        if (supportedPort == null) {
            LOG.warn("Error updating supported port with interface name - port does not exist in device configuration");
            return;
        }
        List<Interfaces> supportInterList = new ArrayList<>();
        if (supportedPort.getInterfaces() != null) {
            supportInterList = new ArrayList<>(supportedPort.getInterfaces());
        }
        if ((supportInterList == null || supportInterList.isEmpty()) && isDeleting) {
            LOG.warn("Error deleting interface {} from port {} of circuit-pack {}. Interface does not exist.",
                interf.getName(),
                interf.getSupportingPort(), interf.getSupportingCircuitPackName());
            return;
        }
        Interfaces portInterfaces = new InterfacesBuilder().setInterfaceName(interf.getName()).build();
        if (!supportInterList.contains(portInterfaces) && !isDeleting) {
            LOG.info("updating circuit-pack {} port {} interface list with {}", interf.getSupportingCircuitPackName(),
                interf.getSupportingPort(), interf.getName());
            supportInterList.add(portInterfaces);
        }
        if (supportInterList.contains(portInterfaces) && isDeleting) {
            LOG.info("deleting from circuit-pack {} port {} interface {}", interf.getSupportingCircuitPackName(),
                interf.getSupportingPort(), interf.getName());
            supportInterList.remove(portInterfaces);
        }
        Ports portAfter = new PortsBuilder(supportedPort).setInterfaces(supportInterList).build();
        deviceUtils.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, supportedPortId, portAfter, null);
    }
}
