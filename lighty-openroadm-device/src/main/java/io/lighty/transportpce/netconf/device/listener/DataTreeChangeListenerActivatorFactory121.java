package io.lighty.transportpce.netconf.device.listener;

import org.opendaylight.mdsal.binding.api.DataBroker;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.OrgOpenroadmDeviceData;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.circuit.pack.Ports;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.circuit.packs.CircuitPacks;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.interfaces.grp.Interface;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.org.openroadm.device.container.OrgOpenroadmDevice;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.org.openroadm.device.container.org.openroadm.device.Info;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.org.openroadm.device.container.org.openroadm.device.RoadmConnections;
import org.opendaylight.yangtools.concepts.Registration;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lighty.transportpce.netconf.device.openroadm.DeviceUtils;
import io.lighty.transportpce.netconf.device.openroadm.rpcs.OrgOpenroadmDeviceServiceImpl121;

public class DataTreeChangeListenerActivatorFactory121 {

    private static final Logger LOG = LoggerFactory.getLogger(DataTreeChangeListenerActivatorFactory121.class);
    private static final InstanceIdentifier<Info> INFO_IID = InstanceIdentifier
        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class).child(Info.class).build();
    private static final InstanceIdentifier<RoadmConnections> ROADM_CONNECTIONS_IID = InstanceIdentifier
        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class).child(RoadmConnections.class)
        .build();
    private static final InstanceIdentifier<CircuitPacks> CIRCUIT_PACKS_IID = InstanceIdentifier
        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class).child(CircuitPacks.class).build();
    private static final InstanceIdentifier<Interface> INTERFACE_IID = InstanceIdentifier
        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class).child(Interface.class).build();
    private static final InstanceIdentifier<Ports> PORTS_IID = InstanceIdentifier
        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class).child(CircuitPacks.class)
        .child(Ports.class).build();

    private final DataBroker dataBroker;
    private final DeviceUtils deviceUtils;
    private final OrgOpenroadmDeviceServiceImpl121 openRoadmDeviceService;
    private Registration dataTreeChangeInfoListenerRegistration;
    private Registration dataTreeChangeRoadmConnectionListenerRegistration;
    private Registration dataTreeChangeCircuitPackChangeListener;
    private Registration dataTreeChangeInterfaceChangeListener;
    private Registration dataTreeChangePortChangeListener;

    public DataTreeChangeListenerActivatorFactory121(final DataBroker dataBroker, final DeviceUtils deviceUtils,
            final OrgOpenroadmDeviceServiceImpl121 openRoadmDeviceService) {
        this.dataBroker = dataBroker;
        this.deviceUtils = deviceUtils;
        this.openRoadmDeviceService = openRoadmDeviceService;
    }

    public void init() {
        DeviceRoadmInfoChangeListener121 deviceRoadmInfoChangeListener = 
            new DeviceRoadmInfoChangeListener121(deviceUtils);
        DeviceRoadmConnectionChangeListener121 deviceRoadmRoadmConnectionChangeListener =
            new DeviceRoadmConnectionChangeListener121(deviceUtils);
        DeviceCircuitPackChangeListener121 deviceRoadmCircuitPackChangeListener =
            new DeviceCircuitPackChangeListener121(deviceUtils);
        DeviceInterfaceChangeListener121 deviceInterfaceChangeListener =
            new DeviceInterfaceChangeListener121(deviceUtils);
        DevicePortChangeListener121 devicePortChangeListener = new DevicePortChangeListener121(deviceUtils);
        devicePortChangeListener.setNotificationPublishService(openRoadmDeviceService.getNotificationPublishService());
        dataTreeChangeInfoListenerRegistration = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.CONFIGURATION, INFO_IID),
                deviceRoadmInfoChangeListener);
        dataTreeChangeRoadmConnectionListenerRegistration = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.CONFIGURATION, ROADM_CONNECTIONS_IID),
                deviceRoadmRoadmConnectionChangeListener);
        dataTreeChangeCircuitPackChangeListener = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.CONFIGURATION, CIRCUIT_PACKS_IID),
                deviceRoadmCircuitPackChangeListener);
        dataTreeChangeInterfaceChangeListener = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.CONFIGURATION, INTERFACE_IID),
                deviceInterfaceChangeListener);
        dataTreeChangePortChangeListener = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.CONFIGURATION, PORTS_IID),
                devicePortChangeListener);

        LOG.info("Data tree change listeners registered");
    }

    public void close() {
        dataTreeChangeInfoListenerRegistration.close();
        dataTreeChangeRoadmConnectionListenerRegistration.close();
        dataTreeChangeCircuitPackChangeListener.close();
        dataTreeChangeInterfaceChangeListener.close();
        dataTreeChangePortChangeListener.close();
    }
}