package io.lighty.transportpce.netconf.device.listener;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.opendaylight.mdsal.binding.api.DataObjectModification;
import org.opendaylight.mdsal.binding.api.DataTreeChangeListener;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.mdsal.binding.api.DataTreeModification;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.circuit.packs.CircuitPacks;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.circuit.packs.CircuitPacksBuilder;
import org.opendaylight.yangtools.yang.binding.DataObjectStep;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lighty.transportpce.netconf.device.openroadm.DeviceUtils;

/**
 * The listener interface for receiving deviceCircuitPackChange events.
 * The class that is interested in processing a deviceCircuitPackChange
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's addDeviceCircuitPackChangeListener method. When
 * the deviceCircuitPackChange event occurs, that object's appropriate
 * method is invoked.
 */
public class DeviceCircuitPackChangeListener221 implements DataTreeChangeListener<CircuitPacks> {

    private static final Logger LOG = LoggerFactory.getLogger(DeviceCircuitPackChangeListener221.class);
    private DeviceUtils deviceUtils;

    public DeviceCircuitPackChangeListener221(DeviceUtils deviceUtils) {
        this.deviceUtils = deviceUtils;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onDataTreeChanged(@NonNull List<DataTreeModification<CircuitPacks>> changes) {
        LOG.info("onDataTreeChanged - CircuitPacks");
        for (DataTreeModification<CircuitPacks> change : changes) {
            final DataObjectModification<CircuitPacks> rootNode = change.getRootNode();
            final DataTreeIdentifier<CircuitPacks> rootPath = change.getRootPath();

            if (rootNode != null && rootNode.dataBefore() != null
                && !rootNode.dataBefore().equals(rootNode.dataAfter())
                && isRealCircuitPackModif(rootNode)) {
                LOG.debug("Received Circuit-pack change({}):\n dataType={} \n before={} \n after={} \n identifier = {}",
                    rootNode.modificationType(), rootNode.dataType(), rootNode.dataBefore(),
                    rootNode.dataAfter(), rootNode.step());
                InstanceIdentifier<CircuitPacks> cpId = null;
                if (rootPath.path().getTargetType().getName().equals(CircuitPacks.class.getName())) {
                    LinkedList<DataObjectStep<?>> path = new LinkedList<>();
                    rootPath.path().getPathArguments().forEach(p -> path.add(p));
                    cpId = InstanceIdentifier.unsafeOf(path);
                }
                switch (rootNode.modificationType()) {
                case SUBTREE_MODIFIED:
                case WRITE:
                    LOG.info("circuit-pack is being modified");
                    updateCircuitPackOnOperationalDS(rootNode.dataBefore(), rootNode.dataAfter(), cpId);
                    break;
                case DELETE:
                    LOG.info("circuit-pack is being deleted");
                    break;
                default:
                    break;
                }
            }
        }
    }

    private synchronized void updateCircuitPackOnOperationalDS(CircuitPacks cpBefore, CircuitPacks cpAfter,
        InstanceIdentifier<CircuitPacks> cpId) {
        if (cpId == null) {
            LOG.warn("Error to update circuit-pack in operational DS - circuit-pack instance id is null");
            return;
        }
        CircuitPacks oldCircuitPack = deviceUtils.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, cpId);
        CircuitPacksBuilder cpBldr = new CircuitPacksBuilder(oldCircuitPack);
        if ((cpBefore.getAdministrativeState() != null && cpAfter.getAdministrativeState() != null
            && !cpBefore.getAdministrativeState().equals(cpAfter.getAdministrativeState()))
            || (cpBefore.getAdministrativeState() == null && cpAfter.getAdministrativeState() != null)) {
            LOG.info("administrative state of circuit-pack {} changed", cpBefore.getCircuitPackName());
            cpBldr.setAdministrativeState(cpAfter.getAdministrativeState())
                .setOperationalState(deviceUtils.setOperationalStateFromAdminState(cpAfter.getAdministrativeState()));
        }
        if ((cpBefore.getEquipmentState() != null && cpAfter.getEquipmentState() != null
            && !cpBefore.getEquipmentState().equals(cpAfter.getEquipmentState()))
            || (cpBefore.getEquipmentState() == null && cpAfter.getEquipmentState() != null)) {
            LOG.info("equipment state of circuit-pack {} changed", cpBefore.getCircuitPackName());
            cpBldr.setEquipmentState(cpAfter.getEquipmentState());
        }
        deviceUtils.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, cpId, cpBldr.build(), null);
    }

    private boolean isRealCircuitPackModif(DataObjectModification<CircuitPacks> rootNode) {
        if (rootNode.dataAfter() == null || rootNode.dataBefore() == null) {
            return false;
        }
        LOG.debug("Verifying if real modified children for CircuitPacks {}",
            rootNode.dataBefore().getCircuitPackName());
        CircuitPacks cpBefore = new CircuitPacksBuilder(rootNode.dataBefore())
            .setPorts(null)
            .build();
        CircuitPacks cpAfter = new CircuitPacksBuilder(rootNode.dataAfter())
            .setPorts(null)
            .build();
        return !cpAfter.equals(cpBefore);
    }
}
