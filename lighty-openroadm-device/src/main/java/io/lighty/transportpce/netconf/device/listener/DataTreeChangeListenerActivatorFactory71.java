package io.lighty.transportpce.netconf.device.listener;

import org.opendaylight.mdsal.binding.api.DataBroker;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.OrgOpenroadmDeviceData;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.pack.Ports;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.packs.CircuitPacks;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.interfaces.grp.Interface;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.org.openroadm.device.container.OrgOpenroadmDevice;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.org.openroadm.device.container.org.openroadm.device.Info;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.org.openroadm.device.container.org.openroadm.device.OduConnection;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.org.openroadm.device.container.org.openroadm.device.RoadmConnections;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.CurrentPmList;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.group.CurrentPm;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.list.CurrentPmEntry;
import org.opendaylight.yangtools.concepts.Registration;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lighty.transportpce.netconf.device.openroadm.DeviceUtils;
import io.lighty.transportpce.netconf.device.openroadm.rpcs.OrgOpenroadmDeviceServiceImpl71;

public class DataTreeChangeListenerActivatorFactory71 {

    private static final Logger LOG = LoggerFactory.getLogger(DataTreeChangeListenerActivatorFactory71.class);
    private static final InstanceIdentifier<Info> INFO_IID = InstanceIdentifier
        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class).child(Info.class).build();
    private static final InstanceIdentifier<RoadmConnections> ROADM_CONNECTIONS_IID = InstanceIdentifier
        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class).child(RoadmConnections.class)
        .build();
    private static final InstanceIdentifier<CircuitPacks> CIRCUIT_PACKS_IID = InstanceIdentifier
        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class).child(CircuitPacks.class).build();
    private static final InstanceIdentifier<Interface> INTERFACE_IID = InstanceIdentifier
        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class).child(Interface.class).build();
    private static final InstanceIdentifier<OduConnection> ODU_CONNECTION_IID = InstanceIdentifier
        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class).child(OduConnection.class).build();
    private static final InstanceIdentifier<Ports> PORTS_IID = InstanceIdentifier
        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class).child(CircuitPacks.class)
        .child(Ports.class).build();
    private static final InstanceIdentifier<CurrentPm> PM_IID = InstanceIdentifier.create(CurrentPmList.class)
        .child(CurrentPmEntry.class).child(CurrentPm.class);

    private final DataBroker dataBroker;
    private final DeviceUtils deviceUtils;
    private final OrgOpenroadmDeviceServiceImpl71 openRoadmDeviceService;
    private Registration dataTreeChangeInfoListenerRegistration;
    private Registration dataTreeChangeRoadmConnectionListenerRegistration;
    private Registration dataTreeChangeCircuitPackChangeListener;
    private Registration dataTreeChangeInterfaceChangeListener;
    private Registration dataTreeChangeOduConnectionChangeListener;
    private Registration dataTreeChangePortChangeListener;
    private Registration dataTreeChangePmListener;

    public DataTreeChangeListenerActivatorFactory71(final DataBroker dataBroker, final DeviceUtils deviceUtils,
            final OrgOpenroadmDeviceServiceImpl71 openRoadmDeviceService) {
        this.dataBroker = dataBroker;
        this.deviceUtils = deviceUtils;
        this.openRoadmDeviceService = openRoadmDeviceService;
    }

    public void init() {
        DeviceRoadmInfoChangeListener71 deviceRoadmInfoChangeListener = 
            new DeviceRoadmInfoChangeListener71(deviceUtils);
        DeviceRoadmConnectionChangeListener71 deviceRoadmRoadmConnectionChangeListener =
            new DeviceRoadmConnectionChangeListener71(deviceUtils);
        DeviceCircuitPackChangeListener71 deviceRoadmCircuitPackChangeListener =
            new DeviceCircuitPackChangeListener71(deviceUtils);
        DeviceInterfaceChangeListener71 deviceInterfaceChangeListener =
            new DeviceInterfaceChangeListener71(deviceUtils);
        DeviceOduConnectionChangeListener71 deviceOduConnectionChangeListener =
            new DeviceOduConnectionChangeListener71(deviceUtils);
        DevicePortChangeListener71 devicePortChangeListener = new DevicePortChangeListener71(deviceUtils);
        CurrentPmChangeListener71 currentPmChangeListener = new CurrentPmChangeListener71(deviceUtils);
        dataTreeChangeInfoListenerRegistration = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.CONFIGURATION, INFO_IID),
                deviceRoadmInfoChangeListener);
        dataTreeChangeRoadmConnectionListenerRegistration = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.CONFIGURATION,
                ROADM_CONNECTIONS_IID), deviceRoadmRoadmConnectionChangeListener);
        dataTreeChangeCircuitPackChangeListener = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.CONFIGURATION, CIRCUIT_PACKS_IID),
                deviceRoadmCircuitPackChangeListener);
        dataTreeChangeInterfaceChangeListener = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.CONFIGURATION, INTERFACE_IID),
                deviceInterfaceChangeListener);
        dataTreeChangeOduConnectionChangeListener = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.CONFIGURATION, ODU_CONNECTION_IID),
                deviceOduConnectionChangeListener);
        dataTreeChangePortChangeListener = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.CONFIGURATION, PORTS_IID),
                devicePortChangeListener);
        dataTreeChangePmListener = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.OPERATIONAL, PM_IID),
                currentPmChangeListener);

        LOG.info("Data tree change listeners registered");
    }

    public void close() {
        dataTreeChangeInfoListenerRegistration.close();
        dataTreeChangeRoadmConnectionListenerRegistration.close();
        dataTreeChangeCircuitPackChangeListener.close();
        dataTreeChangeInterfaceChangeListener.close();
        dataTreeChangeOduConnectionChangeListener.close();
        dataTreeChangePortChangeListener.close();
        dataTreeChangePmListener.close();
    }
}