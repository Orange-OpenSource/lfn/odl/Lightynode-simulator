package io.lighty.transportpce.netconf.device.listener;

import org.opendaylight.mdsal.binding.api.DataBroker;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.OrgOpenroadmDeviceData;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.circuit.pack.Ports;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.circuit.packs.CircuitPacks;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.interfaces.grp.Interface;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.org.openroadm.device.container.OrgOpenroadmDevice;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.org.openroadm.device.container.org.openroadm.device.Info;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.org.openroadm.device.container.org.openroadm.device.OduConnection;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.org.openroadm.device.container.org.openroadm.device.RoadmConnections;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev181019.CurrentPmList;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev181019.current.pm.group.CurrentPm;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev181019.current.pm.list.CurrentPmEntry;
import org.opendaylight.yangtools.concepts.Registration;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lighty.transportpce.netconf.device.openroadm.DeviceUtils;
import io.lighty.transportpce.netconf.device.openroadm.rpcs.OrgOpenroadmDeviceServiceImpl221;

public class DataTreeChangeListenerActivatorFactory221 {

    private static final Logger LOG = LoggerFactory.getLogger(DataTreeChangeListenerActivatorFactory221.class);
    private static final InstanceIdentifier<Info> INFO_IID = InstanceIdentifier
        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class).child(Info.class).build();
    private static final InstanceIdentifier<RoadmConnections> ROADM_CONNECTIONS_IID = InstanceIdentifier
        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class).child(RoadmConnections.class)
        .build();
    private static final InstanceIdentifier<CircuitPacks> CIRCUIT_PACKS_IID = InstanceIdentifier
        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class).child(CircuitPacks.class).build();
    private static final InstanceIdentifier<Interface> INTERFACE_IID = InstanceIdentifier
        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class).child(Interface.class).build();
    private static final InstanceIdentifier<OduConnection> ODU_CONNECTION_IID = InstanceIdentifier
        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class).child(OduConnection.class).build();
    private static final InstanceIdentifier<Ports> PORTS_IID = InstanceIdentifier
        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class).child(CircuitPacks.class)
        .child(Ports.class).build();
    private static final InstanceIdentifier<CurrentPm> PM_IID = InstanceIdentifier.create(CurrentPmList.class)
        .child(CurrentPmEntry.class).child(CurrentPm.class);

    private final DataBroker dataBroker;
    private final DeviceUtils deviceUtils;
    private final OrgOpenroadmDeviceServiceImpl221 openRoadmDeviceService;
    private Registration dataTreeChangeInfoListenerRegistration;
    private Registration dataTreeChangeRoadmConnectionListenerRegistration;
    private Registration dataTreeChangeCircuitPackChangeListener;
    private Registration dataTreeChangeInterfaceChangeListener;
    private Registration dataTreeChangeOduConnectionChangeListener;
    private Registration dataTreeChangePortChangeListener;
    private Registration dataTreeChangePmListener;

    public DataTreeChangeListenerActivatorFactory221(final DataBroker dataBroker, final DeviceUtils deviceUtils,
            final OrgOpenroadmDeviceServiceImpl221 openRoadmDeviceService) {
        this.dataBroker = dataBroker;
        this.deviceUtils = deviceUtils;
        this.openRoadmDeviceService = openRoadmDeviceService;
    }

    public void init() {
        DeviceRoadmInfoChangeListener221 deviceRoadmInfoChangeListener = 
            new DeviceRoadmInfoChangeListener221(deviceUtils);
        DeviceRoadmConnectionChangeListener221 deviceRoadmRoadmConnectionChangeListener =
            new DeviceRoadmConnectionChangeListener221(deviceUtils);
        DeviceCircuitPackChangeListener221 deviceRoadmCircuitPackChangeListener =
            new DeviceCircuitPackChangeListener221(deviceUtils);
        DeviceInterfaceChangeListener221 deviceInterfaceChangeListener =
            new DeviceInterfaceChangeListener221(deviceUtils);
        DeviceOduConnectionChangeListener221 deviceOduConnectionChangeListener =
            new DeviceOduConnectionChangeListener221(deviceUtils);
        DevicePortChangeListener221 devicePortChangeListener = new DevicePortChangeListener221(deviceUtils);
        CurrentPmChangeListener221 currentPmChangeListener = new CurrentPmChangeListener221(deviceUtils);
        dataTreeChangeInfoListenerRegistration = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.CONFIGURATION, INFO_IID),
                deviceRoadmInfoChangeListener);
        dataTreeChangeRoadmConnectionListenerRegistration = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.CONFIGURATION,
                ROADM_CONNECTIONS_IID), deviceRoadmRoadmConnectionChangeListener);
        dataTreeChangeCircuitPackChangeListener = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.CONFIGURATION, CIRCUIT_PACKS_IID),
                deviceRoadmCircuitPackChangeListener);
        dataTreeChangeInterfaceChangeListener = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.CONFIGURATION, INTERFACE_IID),
                deviceInterfaceChangeListener);
        dataTreeChangeOduConnectionChangeListener = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.CONFIGURATION, ODU_CONNECTION_IID),
                deviceOduConnectionChangeListener);
        dataTreeChangePortChangeListener = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.CONFIGURATION, PORTS_IID),
                devicePortChangeListener);
        dataTreeChangePmListener = dataBroker
            .registerTreeChangeListener(DataTreeIdentifier.of(LogicalDatastoreType.OPERATIONAL, PM_IID),
                currentPmChangeListener);

        LOG.info("Data tree change listeners registered");
    }

    public void close() {
        dataTreeChangeInfoListenerRegistration.close();
        dataTreeChangeRoadmConnectionListenerRegistration.close();
        dataTreeChangeCircuitPackChangeListener.close();
        dataTreeChangeInterfaceChangeListener.close();
        dataTreeChangeOduConnectionChangeListener.close();
        dataTreeChangePortChangeListener.close();
        dataTreeChangePmListener.close();
    }
}