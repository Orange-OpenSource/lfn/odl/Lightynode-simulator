/*
 * Copyright (c) 2020 PANTHEON.tech s.r.o. All Rights Reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-v10.html
 */
package io.lighty.transportpce.netconf.device.openroadm.processors;

import java.util.concurrent.Future;

import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.GetConnectionPortTrail;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.GetConnectionPortTrailInput;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.GetConnectionPortTrailOutput;
import org.opendaylight.yangtools.yang.common.QName;
import org.opendaylight.yangtools.yang.common.RpcResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.ListenableFuture;

import io.lighty.transportpce.netconf.device.openroadm.rpcs.OrgOpenroadmDeviceServiceImpl221;

@SuppressWarnings("checkstyle:MemberName")
public class OpenroadmServiceGetConnectionPortTrailProcessor221 extends OpenroadmServiceAbstractProcessor
        <GetConnectionPortTrailInput, GetConnectionPortTrailOutput> implements GetConnectionPortTrail {

    private static final Logger LOG = LoggerFactory.getLogger(OpenroadmServiceGetConnectionPortTrailProcessor221.class);

    private final OrgOpenroadmDeviceServiceImpl221 openRoadmDeviceService;
    private final QName qName = QName.create("http://org/openroadm/device", "get-connection-port-trail");

    public OpenroadmServiceGetConnectionPortTrailProcessor221(OrgOpenroadmDeviceServiceImpl221 openRoadmDeviceService) {
        this.openRoadmDeviceService = openRoadmDeviceService;
    }

    @Override
    public QName getIdentifier() {
        return this.qName;
    }

    @Override
    protected Future<RpcResult<GetConnectionPortTrailOutput>> execMethod(GetConnectionPortTrailInput input) {
        LOG.info("execute RPC: GetConnectionPortTrail");
        return openRoadmDeviceService.getConnectionPortTrail(input);
    }

    @Override
    public ListenableFuture<RpcResult<GetConnectionPortTrailOutput>> invoke(GetConnectionPortTrailInput input) {
        // TODO Auto-generated method stub
        return null;
    }
}
