/*
 * Copyright (c) 2020 PANTHEON.tech s.r.o. All Rights Reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-v10.html
 */
package io.lighty.transportpce.netconf.device.openroadm;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.lighty.applications.util.ModulesConfig;
import io.lighty.core.common.exceptions.ModuleStartupException;
import io.lighty.core.controller.api.LightyController;
import io.lighty.core.controller.impl.LightyControllerBuilder;
import io.lighty.core.controller.impl.config.ConfigurationException;
import io.lighty.core.controller.impl.config.ControllerConfiguration;
import io.lighty.core.controller.impl.util.ControllerConfigUtils;
//import io.lighty.modules.northbound.restconf.community.impl.CommunityRestConf;
//import io.lighty.modules.northbound.restconf.community.impl.CommunityRestConfBuilder;
import io.lighty.modules.northbound.restconf.community.impl.config.RestConfConfiguration;
import io.lighty.modules.northbound.restconf.community.impl.util.RestConfConfigUtils;
import io.lighty.modules.southbound.netconf.impl.NetconfSBPlugin;
import io.lighty.modules.southbound.netconf.impl.NetconfTopologyPluginBuilder;
import io.lighty.modules.southbound.netconf.impl.config.NetconfConfiguration;
import io.lighty.modules.southbound.netconf.impl.util.NetconfConfigUtils;
//import io.lighty.server.LightyServerBuilder;
//import io.lighty.swagger.SwaggerLighty;
import io.lighty.transportpce.device.openroadm.utils.ModelsUtils;
import io.lighty.transportpce.netconf.device.NetconfDevice;
import io.lighty.transportpce.netconf.device.listener.DataTreeChangeListenerActivatorFactory221;
import io.lighty.transportpce.netconf.device.openroadm.rpcs.OrgOpenroadmDeviceServiceImpl221;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
//import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.opendaylight.yangtools.yang.binding.YangModuleInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);
    private ShutdownHook shutdownHook;
    private static String operFile = null;
    private static Path restconfConfig = null;
    private static String deviceType = "netconf";
    private static int netconPort = 17830;
    private static String modelDevice = "2.2.1";

    public static void main(String[] args) throws InterruptedException, ConfigurationException, IOException,
        ExecutionException, TimeoutException, ModuleStartupException {
        cliParser(args);

        Main app = new Main();
        app.start(netconPort, true, true);
    }

    /*
     * Build options for command line arguments.
     */
    private static void cliParser(String[] args) {
        Options cliOption = new Options();
        cliOption.addOption("t", "type", true, "Netconf or Netconf+Restconf device, default value is Netconf");
        cliOption.addOption("p", "port", true, "Netconf port of the device, default value is 17830");
        cliOption.addOption("f", "file", true, "The initial configuration file to use");
        cliOption.addOption("r", "restconf", true, "The restconf configuration file to use");
        cliOption.addOption("v", "version", true, "The version of the device datamodels");
        cliOption.addOption("h", "help", false, "");
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(cliOption, args);
        } catch (ParseException e) {
            LOG.error("Unable to parse command-line options : {}", e.getMessage());
            System.exit(-1);
        }

        if (cmd.hasOption('f')) {
            operFile = cmd.getOptionValue('f');
        }
        if (cmd.hasOption('t')) {
            deviceType = cmd.getOptionValue('t');
        }
        if (cmd.hasOption('p')) {
            netconPort = Integer.parseInt(cmd.getOptionValue('p'));
        }
        if (cmd.hasOption('r')) {
            restconfConfig = Paths.get(cmd.getOptionValue('r'));
            LOG.info("restconf configuration file: {}", restconfConfig.toString());
        }
        if (cmd.hasOption('v')) {
            modelDevice = cmd.getOptionValue('v');
        }
        if (cmd.hasOption('h')) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("CLI", cliOption);
            System.exit(-1);
        }
    }

    @SuppressFBWarnings({ "SLF4J_SIGN_ONLY_FORMAT", "OBL_UNSATISFIED_OBLIGATION" })
    public void start(int initPort, boolean registerShutdownHook, final boolean initDataStore)
        throws InterruptedException, ConfigurationException, IOException, ExecutionException, TimeoutException,
        ModuleStartupException {
        LOG.info("___________             __        ________              .__");
        LOG.info("\\__    ___/___  _______/  |_______\\______ \\   _______  _|__| ____  ____");
        LOG.info("  |    | /  _ \\/  ___/\\   __\\_  __ \\    |  \\_/ __ \\  \\/ /  |/ ___\\/ __ \\");
        LOG.info("  |    |(  <_> )___ \\  |  |  |  | \\/    `   \\  ___/\\   /|  \\  \\__\\  ___/");
        LOG.info("  |____| \\____/____  > |__|  |__| /_______  /\\___  >\\_/ |__|\\___  >___  >");
        LOG.info("                   \\/                     \\/     \\/             \\/    \\/");

        LOG.info("[https://lighty.io]");

        // Get the initial device configuration
        InputStream initialOperationalData = null;
        InputStream initialConfigurationData = null;
        if (operFile != null) {
            try {
                initialOperationalData = new FileInputStream(new File(operFile));
                initialConfigurationData = new FileInputStream(new File(operFile));
            } catch (FileNotFoundException e) {
                LOG.error("Input file configuration {} not found.", e.getMessage());
                System.exit(-1);
            }
        } else {
            LOG.error("Init configuration file option is empty or does not exist");
            System.exit(-1);
        }

        // Start netconf device
        Set<YangModuleInfo> models = null;
        LOG.info("Start OpenRoadmDevice {} on port {} with init configuration file {}", modelDevice, netconPort,
            operFile);
        switch (modelDevice.toLowerCase()) {
        case "1.2.1":
            models = ModelsUtils.getYangModels121();
            OpenRoadmDeviceImpl121 openRoadm121 = new OpenRoadmDeviceImpl121(models, initPort, initDataStore,
                initialOperationalData);
            boolean isOpendRoadm121Started = openRoadm121.initProcedure();
            if (!isOpendRoadm121Started) {
                throw new ModuleStartupException("openRoadm121 netconf device startup failed!");
            }
            break;
        case "7.1":
            models = ModelsUtils.getYangModels71();
            OpenRoadmDeviceImpl71 openRoadm71 = new OpenRoadmDeviceImpl71(models, initPort, initDataStore,
                initialOperationalData);
            boolean isOpendRoadm71Started = openRoadm71.initProcedure();
            if (!isOpendRoadm71Started) {
                throw new ModuleStartupException("openRoadm71 netconf device startup failed!");
            }
            break;
        case "oc":
            models = ModelsUtils.getYangModels240119();
            TerminalDevice240119 oc240119 = new TerminalDevice240119(models, initPort, initDataStore,
                initialOperationalData, initialConfigurationData);
            boolean isoc240119Started = oc240119.initProcedure();
            if (!isoc240119Started) {
                throw new ModuleStartupException("open-config-240119 netconf device startup failed!");
            }
            break;
        default:
            models = ModelsUtils.getYangModels221();
            OpenRoadmDeviceImpl221 openRoadm221 = new OpenRoadmDeviceImpl221(models, initPort, initDataStore,
                initialOperationalData);
            boolean isOpendRoadm221Started = openRoadm221.initProcedure();
            if (!isOpendRoadm221Started) {
                throw new ModuleStartupException("openRoadm221 netconf device startup failed!");
            }
        }

        if (deviceType != "netconf") {
            LOG.info("Start Restconf interface and Swagger UI");
            // Get RESTCONF configuration from "restconfConfig" file
            RestConfConfiguration restconfConfiguration = null;
            ModulesConfig moduleConfiguration = null;
            if (restconfConfig != null) {
                restconfConfiguration = RestConfConfigUtils
                    .getRestConfConfiguration(Files.newInputStream(restconfConfig));
                LOG.info("ip address : {}", restconfConfiguration.getInetAddress().toString());
                LOG.info("Http port : {}", restconfConfiguration.getHttpPort());
                moduleConfiguration = ModulesConfig.getModulesConfig(Files.newInputStream(restconfConfig));
            } else {
                LOG.info("Using default restconf configuration with http port 8181 ...");

                restconfConfiguration = RestConfConfigUtils.getDefaultRestConfConfiguration();
                restconfConfiguration.setHttpPort(8181);
                moduleConfiguration = ModulesConfig.getDefaultModulesConfig();
                moduleConfiguration.setModuleTimeoutSeconds(60);
            }
            // NETCONF SBP configuration
            NetconfConfiguration netconfSBPConfig = NetconfConfigUtils.createDefaultNetconfConfiguration();
            ControllerConfiguration singleNodeConfiguration = ControllerConfigUtils
                .getDefaultSingleNodeConfiguration(models);

            // Initialize and start Lighty controller (MD-SAL, Controller, YangTools, Akka
            // from restconfconfig file)
            LightyControllerBuilder lightyControllerBuilder = new LightyControllerBuilder();
            LightyController lightyController = lightyControllerBuilder.from(singleNodeConfiguration).build();
            boolean isLightControllerStarted = lightyController.start()
                .get(moduleConfiguration.getModuleTimeoutSeconds(), TimeUnit.SECONDS);
            if (!isLightControllerStarted) {
                throw new ModuleStartupException("Lighty.io controller startup failed!");
            }

//            // Start swagger server
//            LightyServerBuilder jettyServerBuilder = new LightyServerBuilder(new InetSocketAddress(
//                restconfConfiguration.getInetAddress(), restconfConfiguration.getHttpPort()));
//            SwaggerLighty swagger = new SwaggerLighty(restconfConfiguration, jettyServerBuilder,
//                lightyController.getServices());
//            boolean swaggerStartOk = swagger.start()
//                .get(moduleConfiguration.getModuleTimeoutSeconds(), TimeUnit.SECONDS);
//            if (!swaggerStartOk) {
//                throw new ModuleStartupException("Lighty.io Swagger startup failed!");
//            }

//            // Start RestConf server
//            CommunityRestConfBuilder communityRestconfBuilder = CommunityRestConfBuilder.from(RestConfConfigUtils
//                .getRestConfConfiguration(restconfConfiguration, lightyController.getServices()));
//            CommunityRestConf communityRestConf = communityRestconfBuilder.withLightyServer(jettyServerBuilder).build();
//            boolean isCommunityRestconfStarted = communityRestConf.start()
//                .get(moduleConfiguration.getModuleTimeoutSeconds(), TimeUnit.SECONDS);
//            if (!isCommunityRestconfStarted) {
//                throw new ModuleStartupException("Lighty.io restconf startup failed!");
//            } else {
//                communityRestConf.startServer();
//            }

            // start NetConf SBP
            NetconfSBPlugin netconfSouthboundPlugin;
            netconfSBPConfig = NetconfConfigUtils.injectServicesToTopologyConfig(
                netconfSBPConfig, lightyController.getServices());
            NetconfTopologyPluginBuilder netconfSBPBuilder = new NetconfTopologyPluginBuilder(
                lightyController.getServices(), netconfSBPConfig);
            netconfSouthboundPlugin = netconfSBPBuilder.from(netconfSBPConfig, lightyController.getServices())
                .build();
            boolean netconfSBPStartOk = netconfSouthboundPlugin.start()
                .get(moduleConfiguration.getModuleTimeoutSeconds(), TimeUnit.SECONDS);
            if (!netconfSBPStartOk) {
                throw new ModuleStartupException("Lighty.io Netconf SBP startup failed!");
            }
        }
    }

    public void shutdown() throws Throwable {
        if (shutdownHook != null) {
            shutdownHook.execute();
        }
    }

    private static class ShutdownHook extends Thread {

        private final NetconfDevice netConfDevice;
        private final OrgOpenroadmDeviceServiceImpl221 openroadmService;

        ShutdownHook(NetconfDevice netConfDevice, OrgOpenroadmDeviceServiceImpl221 openroadmService,
            DataTreeChangeListenerActivatorFactory221 listenerActivator) {
            this.netConfDevice = netConfDevice;
            this.openroadmService = openroadmService;
        }

        @Override
        public void run() {
            try {
                this.execute();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        public void execute() throws Exception {
            LOG.info("Shutting down openROADM device.");
            if (openroadmService != null) {
                openroadmService.close();
            }
            if (netConfDevice != null) {
                try {
                    netConfDevice.close();
                } catch (Exception e) {
                    LOG.error("Failed to close Netconf device properly", e);
                }
            }
        }
    }

    public static String getOperFile() {
        return operFile;
    }
};