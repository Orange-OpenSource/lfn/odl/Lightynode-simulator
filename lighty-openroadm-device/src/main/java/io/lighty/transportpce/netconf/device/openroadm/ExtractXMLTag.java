package io.lighty.transportpce.netconf.device.openroadm;

import java.io.IOException;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExtractXMLTag {
    private static final Logger LOG = LoggerFactory.getLogger(ExtractXMLTag.class);

    private ExtractXMLTag() {

    }

    public static String extractTagElement(String oper_path, String tag, String namespace) throws TransformerException {
        String result = null;
        LOG.info("Getting {} xml data", tag);
        DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
            Document doc = docBuilder.parse(oper_path);
            Document extract = extractDom(doc, tag, namespace);
            if (extract != null) {
                result = getStringFromDocument(extract);
            } else {
                throw new NullPointerException("Failed to extract data from document !");
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            LOG.error("failed to extract data from document", e);
        }
        return result;
    }

    private static String getStringFromDocument(Document extract) throws TransformerException {
        DOMSource domSource = new DOMSource(extract);
        StringWriter write = new StringWriter();
        StreamResult result = new StreamResult(write);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.transform(domSource, result);
        return write.toString();
    }

    private static Document extractDom(Document doc, String tag, String namespace) throws ParserConfigurationException {
        NodeList nodeList = doc.getElementsByTagName(tag);
        if (nodeList.getLength() > 0) {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbf.newDocumentBuilder();
            Document result = builder.newDocument();
            Element root = result.createElementNS("urn:ietf:params:xml:ns:netconf:base:1.0", "data");
            result.appendChild(root);
            Node newNode = result.importNode(nodeList.item(0), true);
            result.getDocumentElement().appendChild(newNode);
            return result;
        }
        return null;
    }

}


