/*
 * Copyright (c) 2020 PANTHEON.tech s.r.o. All Rights Reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-v10.html
 */

package io.lighty.transportpce.netconf.device.openroadm;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Set;

import org.opendaylight.mdsal.binding.api.DataBroker;
import org.opendaylight.yangtools.yang.binding.YangModuleInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lighty.core.controller.api.AbstractLightyModule;
import io.lighty.transportpce.netconf.device.NetconfDevice;
import io.lighty.transportpce.netconf.device.NetconfDeviceBuilder;
import io.lighty.transportpce.netconf.device.listener.DataTreeChangeListenerActivatorFactory121;
import io.lighty.transportpce.netconf.device.openroadm.processors.OpenroadmServiceGetConnectionPortTrailProcessor121;
import io.lighty.transportpce.netconf.device.openroadm.rpcs.OrgOpenroadmDeviceServiceImpl121;
import io.lighty.transportpce.netconf.device.requests.notification.NotificationPublishService;

public class OpenRoadmDeviceImpl121 extends AbstractLightyModule implements OpenRoadmDevice {
    private static final Logger LOG = LoggerFactory.getLogger(OpenRoadmDeviceImpl121.class);
    private DataTreeChangeListenerActivatorFactory121 listenerActivator;

    public OpenRoadmDeviceImpl121 (Set<YangModuleInfo> models, int initPort, boolean initDataStore,
            InputStream initialOperationalData) {
        LOG.info("creating OpenRoadmDeviceImpl 1.2.1");
        OrgOpenroadmDeviceServiceImpl121 openRoadmDeviceService = new OrgOpenroadmDeviceServiceImpl121();
        OpenroadmServiceGetConnectionPortTrailProcessor121 getConnectionPortTrailProcessor =
            new OpenroadmServiceGetConnectionPortTrailProcessor121(openRoadmDeviceService);

        final NetconfDeviceBuilder netconfDeviceBuilder = new NetconfDeviceBuilder()
            .setCredentials("admin", "admin")
            .setBindingPort(initPort)
            .withModels(models)
            .withDefaultRequestProcessors()
            .withDefaultCapabilities()
            .withDefaultNotificationProcessor()
            .withNetconfMonitoringEnabled(true)
            .withRequestProcessor(getConnectionPortTrailProcessor);

        if (initDataStore) {
            InputStream initialConfigurationData = new ByteArrayInputStream(
                DeviceConfiguration.loadConfigData(DeviceUtils.CONFIG_XSL_121).getBytes());

            netconfDeviceBuilder.setInitialOperationalData(initialOperationalData)
                .setInitialConfigurationData(initialConfigurationData);
        }
        // Initialize NetconfDevice and start it from netConfDeviceSimulator
        NetconfDevice netconfDevice = netconfDeviceBuilder.build();
        NotificationPublishService notifPublishService = netconfDevice.getNetconfDeviceServices()
            .getNotificationPublishService();
        LOG.info(notifPublishService.toString());
        openRoadmDeviceService.setNotificationPublishService(netconfDevice.getNetconfDeviceServices()
            .getNotificationPublishService());

        netconfDevice.start();
        DataBroker dataBroker = netconfDevice.getNetconfDeviceServices().getDataBroker();
        DeviceUtils deviceUtils = new DeviceUtils(dataBroker, notifPublishService);
        openRoadmDeviceService.setDataBrokerService(deviceUtils);

        listenerActivator = new DataTreeChangeListenerActivatorFactory121(dataBroker,
            deviceUtils, openRoadmDeviceService);
    }

    @Override
    protected boolean initProcedure() throws InterruptedException {
        LOG.info("Initializing OpenRoadmDeviceImpl 1.2.1");
        listenerActivator.init();
        return true;
    }

    @Override
    protected boolean stopProcedure() throws InterruptedException {
        LOG.info("Stoping OpenRoadmDeviceImpl 1.2.1");
        listenerActivator.close();
        return true;
    }
}
