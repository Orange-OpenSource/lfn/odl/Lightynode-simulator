package io.lighty.transportpce.netconf.device.listener;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

import org.opendaylight.mdsal.binding.api.*;
import org.opendaylight.mdsal.common.api.CommitInfo;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.common.node.types.rev191129.NodeIdType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.OrgOpenroadmDeviceData;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.org.openroadm.device.container.OrgOpenroadmDevice;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.org.openroadm.device.container.org.openroadm.device.Info;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.org.openroadm.device.container.org.openroadm.device.InfoBuilder;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import io.lighty.transportpce.netconf.device.openroadm.DeviceUtils;

/**
 * The listener interface for receiving deviceRoadmInfoChange events.
 * The class that is interested in processing a deviceRoadmInfoChange
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's addDeviceRoadmInfoChangeListener method. When
 * the deviceRoadmInfoChange event occurs, that object's appropriate
 * method is invoked.
 */
public class DeviceRoadmInfoChangeListener71 implements DataTreeChangeListener<Info> {
    private static final Logger LOG = LoggerFactory.getLogger(DeviceRoadmInfoChangeListener71.class);
    private DeviceUtils deviceUtils;
    private final ListeningExecutorService executor;

    public DeviceRoadmInfoChangeListener71 (DeviceUtils deviceUtils) {
        this.deviceUtils = deviceUtils;
        this.executor = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(5));
    }

    @Override
    public void onDataTreeChanged(List<DataTreeModification<Info>> changes) {
        LOG.info("onDataTreeChanged - Info");
        for (DataTreeModification<Info> dataTreeModification : changes) {
            DataObjectModification<Info> rootNode = dataTreeModification.getRootNode();
            DataTreeIdentifier<Info> rootPath = dataTreeModification.getRootPath();
            LOG.info("Received Info change({}):\n dataType={} \n before={} \n after={} \n identifier = {}",
                rootNode.modificationType(), rootNode.dataType(), rootNode.dataBefore(),
                rootNode.dataAfter(), rootNode.step());

            if (rootNode != null && rootNode.dataBefore() != null
                && !rootNode.dataBefore().equals(rootNode.dataAfter())) {
                InstanceIdentifier<Info> infoIId = null;
                if (rootPath.path().getTargetType().getName().equals(Info.class.getName())) {
                    infoIId = InstanceIdentifier
                        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class)
                        .child(Info.class).build();
                }

                switch (rootNode.modificationType()) {
                case SUBTREE_MODIFIED:
                case WRITE:
                    LOG.info("info is being modified");
                    updateInfoOnOperationalDS(rootNode.dataBefore(), rootNode.dataAfter(), infoIId);
                    break;
                case DELETE:
                    LOG.info("info is being deleted");
                    deleteInfoOnoperationalDS(LogicalDatastoreType.OPERATIONAL, infoIId);
                    break;
                default:
                    break;
                }
            }
        }
    }

    private void updateInfoOnOperationalDS(Info infoBefore, Info infoAfter, InstanceIdentifier<Info> infoId) {
        if (infoId == null) {
            LOG.warn("Error to create or update info in operational DS - Info instance id is null");
            return;
        }
        ReadWriteTransaction readWriteTransaction = deviceUtils.getDataBroker().newReadWriteTransaction();
        try {
            Optional<Info> optional = readWriteTransaction.read(LogicalDatastoreType.OPERATIONAL, infoId).get();
            if (optional.isPresent()) {
                Info oldInfo = optional.get();
                InfoBuilder infoBldr = new InfoBuilder(oldInfo);
                if ((infoBefore.getNodeId().getValue() != null && infoAfter.getNodeId().getValue() != null
                        && !infoBefore.getNodeId().getValue().equals(infoAfter.getNodeId().getValue()))
                        || (infoBefore.getNodeId().getValue() == null && infoAfter.getNodeId().getValue() != null)) {
                    LOG.info("Before change {} -- After change {}", infoBefore.getNodeId().getValue(),
                        infoAfter.getNodeId().getValue());
                    infoBldr.setNodeId(new NodeIdType(infoAfter.getNodeId()));
                }
                readWriteTransaction.merge(LogicalDatastoreType.OPERATIONAL, infoId, infoBldr.build());
                LOG.info("writing container {} into {} datastore ...", infoId.getTargetType().getSimpleName(),
                    LogicalDatastoreType.OPERATIONAL);
                Futures.addCallback(readWriteTransaction.commit(), new FutureCallback<CommitInfo>() {
                    public void onSuccess(CommitInfo result) {
                        // succeeded
                    }
                    public void onFailure(Throwable t) {
                        // failed due to another type of TransactionCommitFailedException.
                    }
                }, executor);
            }
        } catch (InterruptedException | ExecutionException e) {
            LOG.error("Failed to process WriteTransaction", e);
            Thread.currentThread().interrupt();
        }
    }

    private void deleteInfoOnoperationalDS(LogicalDatastoreType datastore,
        InstanceIdentifier<Info> infoId) {
        if (infoId == null) {
            LOG.warn("Error to copy connection in operational DS - connection instance id is null");
            return;
        }
        ReadWriteTransaction readWriteTransaction = deviceUtils.getDataBroker().newReadWriteTransaction();
        try {
            Optional<Info> info = readWriteTransaction.read(LogicalDatastoreType.OPERATIONAL, infoId)
                .get();
            if (info != null) {
                LOG.info("deleting container {} from {} datastore ...", infoId.getTargetType().getSimpleName(),
                    datastore);
                readWriteTransaction.delete(datastore, infoId);
                readWriteTransaction.commit();
            }
        } catch (InterruptedException | ExecutionException e) {
            LOG.error("Failed to process DeleteTransaction", e);
            Thread.currentThread().interrupt();
        }
    }
}