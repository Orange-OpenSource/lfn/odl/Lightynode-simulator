/*
 * Copyright (c) 2020 PANTHEON.tech s.r.o. All Rights Reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-v10.html
 */
package io.lighty.transportpce.netconf.device.openroadm.rpcs;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;

import io.lighty.transportpce.netconf.device.listener.DeviceRoadmInfoChangeListener221;
import io.lighty.transportpce.netconf.device.openroadm.DeviceUtils;
import io.lighty.transportpce.netconf.device.requests.notification.NotificationPublishService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.HoneynodeRpcActions;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.PmInteractInput;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.PmInteractOutput;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.PmInteractOutputBuilder;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.pm.interact.input.PmToBeSetOrCreated;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.pm.interact.input.PmToGetClearOrDelete;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.pm.interact.output.RetrievedPmResultsBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.common.types.rev200529.RpcStatus;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.GetConnectionPortTrailInput;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.GetConnectionPortTrailOutput;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.GetConnectionPortTrailOutputBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.get.connection.port.trail.output.Ports;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.get.connection.port.trail.output.PortsBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.CurrentPmList;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.CurrentPmListBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.group.CurrentPm;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.group.CurrentPmKey;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.list.CurrentPmEntry;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.list.CurrentPmEntryBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.list.CurrentPmEntryKey;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.val.group.Measurement;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.opendaylight.yangtools.yang.common.RpcResult;
import org.opendaylight.yangtools.yang.common.RpcResultBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrgOpenroadmDeviceServiceImpl71 implements AutoCloseable {

    private static final Logger LOG = LoggerFactory.getLogger(OrgOpenroadmDeviceServiceImpl71.class);

    private final ExecutorService executor;
    private NotificationPublishService notificationPublishService;

    public NotificationPublishService getNotificationPublishService() {
        return notificationPublishService;
    }


    public void setNotificationPublishService(NotificationPublishService notificationPublishService) {
        this.notificationPublishService = notificationPublishService;
    }

    private DeviceUtils deviceUtils;

    private static final String EMPTY_CURRENT_PM_LIST_MSG = "There is no CurrentPmList in DataStore";
    private static final String EMPTY_INTERFACE_MSG = "The resource instance doesn't exist";
    private static final String INVALID_CURRENT_PM_ENTRY_MSG = "The inputs of CurrentPmEntry are incorrect";
    private static final String INVALID_CURRENT_PM_MSG = "The inputs of CurrentPm are incorrect";
    private static final String INVALID_MEASUREMENT = "The inputs of Measurement are incorrect";
    private static final String INVALID_PM_GET_CLEAR_OR_DELETE = "The input pm-to-get-clear-or-delete is null";
    private static final String INVALID_PM_BE_SET_OR_CREATED = "The input pm-to-be-set-or-created is null";

    // Contains the CurrentPm that have been _SET
    public static Map<InstanceIdentifier<CurrentPm>, CurrentPm> setCurrentPmMap = new HashMap<>();

    public OrgOpenroadmDeviceServiceImpl71() {
        this.executor = Executors.newFixedThreadPool(1);
    }

    public void setInfoListenerService(DeviceRoadmInfoChangeListener221 opendRoadmInfoListener) {
    }

    public void setDataBrokerService(final DeviceUtils deviceUtils) {
        this.deviceUtils = deviceUtils;
    }

    public ListenableFuture<RpcResult<GetConnectionPortTrailOutput>> getConnectionPortTrail(
            GetConnectionPortTrailInput input) {
        LOG.info("RPC GetConnectionPortTrail request received !");
        final SettableFuture<RpcResult<GetConnectionPortTrailOutput>> result = SettableFuture.create();
        this.executor.submit(new Callable<RpcResult<GetConnectionPortTrailOutput>>() {
            Ports port = new PortsBuilder()
                .setCircuitPackName("2/0")
                .setPortName("L1")
                .build();
            @Override
            public RpcResult<GetConnectionPortTrailOutput> call() throws Exception {
                final GetConnectionPortTrailOutput output = new GetConnectionPortTrailOutputBuilder()
                    .setPorts(Arrays.asList(port))
                    .setStatusMessage("OK")
                    .setStatus(RpcStatus.Successful)
                    .build();
                final RpcResult<GetConnectionPortTrailOutput> rpcResult = RpcResultBuilder.success(output).build();
                result.set(rpcResult);
                return rpcResult;
            }
        });
        return result;
    }

    public ListenableFuture<RpcResult<PmInteractOutput>> pmInteract(PmInteractInput pmInteractInput) {
        LOG.info("RPC pmInteract request received !");
        final SettableFuture<RpcResult<PmInteractOutput>> result = SettableFuture.create();
        this.executor.submit(new Callable<RpcResult<PmInteractOutput>>() {

            @Override
            public RpcResult<PmInteractOutput> call() throws Exception {
                final PmInteractOutputBuilder output = new PmInteractOutputBuilder();
                RpcResult<PmInteractOutput> rpcResult;
                // Compliance checks
                if (!validateInput(pmInteractInput, output)) {
                    rpcResult = RpcResultBuilder
                        .success(output.build()).build();
                    result.set(rpcResult);
                    return rpcResult;
                }
                LOG.info("rpc-action _{}", pmInteractInput.getRpcAction());

                List<InstanceIdentifier<CurrentPm>> iidCurrentPmArray = new ArrayList<>();

                InstanceIdentifier<CurrentPmList> iidCurrentPmList = InstanceIdentifier.create(CurrentPmList.class);
                Map<CurrentPmEntryKey, CurrentPmEntry> newCurrentPmEntry = new HashMap<>();
                CurrentPmListBuilder newCurrentPmList = new CurrentPmListBuilder();
                CurrentPmList currentPmListDS;
                CurrentPmEntry currentPmEntryDS;

                switch (pmInteractInput.getRpcAction()) {
                case Get:
                    currentPmListDS = deviceUtils.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL,
                        iidCurrentPmList);
                    Map<org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.pm.interact
                        .output.retrieved.pm.results.CurrentPmEntryKey, org.opendaylight.yang.gen.v1.http.
                        honeynode.simulator.pm.handling.rev230629.pm.interact.output.retrieved.pm.results
                        .CurrentPmEntry> currentPmEntries = new HashMap<>();
                    if (currentPmListDS != null) {
                        if (pmInteractInput.getPmToGetClearOrDelete() == null ||
                            pmInteractInput.getPmToGetClearOrDelete().getCurrentPmEntry() == null ||
                            pmInteractInput.getPmToGetClearOrDelete().getCurrentPmEntry().isEmpty()) {
                            // Get all the CurrentPmEntry
                            for (CurrentPmEntry currentPmEntryElem : currentPmListDS.getCurrentPmEntry().values())
                                currentPmEntries.put(new org.opendaylight.yang.gen.v1.http.honeynode.simulator
                                        .pm.handling.rev230629.pm.interact.output.retrieved.pm.results
                                        .CurrentPmEntryKey(currentPmEntryElem.getPmResourceInstance(),
                                            currentPmEntryElem.getPmResourceType(),
                                            currentPmEntryElem.getPmResourceTypeExtension()),
                                    new org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629
                                        .pm.interact.output.retrieved.pm.results
                                        .CurrentPmEntryBuilder(currentPmEntryElem).build());
                        } else {
                            // Get only the CurrentPmEntry specified in the input
                            for (org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629
                                    .pm.interact.input.pm.to.get.clear.or.delete
                                    .CurrentPmEntry currentPmEntryElem : pmInteractInput.getPmToGetClearOrDelete()
                                        .getCurrentPmEntry().values()) {
                                currentPmEntryDS = getCurrentPmEntry(new ArrayList<>(currentPmListDS.getCurrentPmEntry()
                                    .values()), currentPmEntryElem);
                                if (currentPmEntryDS == null) {
                                    LOG.warn("CurrentPmEntry {} doesn't exist", currentPmEntryElem);
                                    output.setStatus(RpcStatus.Failed)
                                        .setStatusMessage("At least one CurrentPmEntry doesn't exist");
                                    rpcResult = RpcResultBuilder
                                        .success(output.build()).build();
                                    result.set(rpcResult);
                                    return rpcResult;
                                }
                                currentPmEntries.put(new org.opendaylight.yang.gen.v1.http.honeynode.simulator
                                        .pm.handling.rev230629.pm.interact.output.retrieved.pm.results
                                        .CurrentPmEntryKey(currentPmEntryElem.getPmResourceInstance(),
                                            currentPmEntryElem.getPmResourceType(),
                                            currentPmEntryElem.getPmResourceTypeExtension()) ,
                                    new org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629
                                        .pm.interact.output.retrieved.pm.results
                                        .CurrentPmEntryBuilder(currentPmEntryDS).build());
                            }
                        }
                        LOG.info("The PMs has been successfully retrieved !");
                        output.setStatus(RpcStatus.Successful)
                        .setStatusMessage("The PMs has been successfully retrieved !")
                        .setRetrievedPmResults(new RetrievedPmResultsBuilder().setCurrentPmEntry(currentPmEntries).build());
                        rpcResult = RpcResultBuilder
                            .success(output.build()).build();
                        result.set(rpcResult);
                        break;

                    } else {
                        output.setStatus(RpcStatus.Failed).setStatusMessage(EMPTY_CURRENT_PM_LIST_MSG);
                        rpcResult = RpcResultBuilder
                            .success(output.build()).build();
                        result.set(rpcResult);
                        return rpcResult;
                    }

                case Set:
                    Map<InstanceIdentifier<CurrentPm>, CurrentPm> tempSetCurrentPmMap = new HashMap<>();
                    currentPmListDS = deviceUtils.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL,
                        iidCurrentPmList);
                    if (currentPmListDS == null) {
                        LOG.warn(EMPTY_CURRENT_PM_LIST_MSG);
                        output.setStatus(RpcStatus.Failed)
                            .setStatusMessage(EMPTY_CURRENT_PM_LIST_MSG);
                        rpcResult = RpcResultBuilder
                            .success(output.build()).build();
                        result.set(rpcResult);
                        return rpcResult;
                    }
                    for (org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.pm.interact.input
                        .pm.to.be.set.or.created.CurrentPmEntry currentPmEntryElem : pmInteractInput
                            .getPmToBeSetOrCreated().getCurrentPmEntry().values()) {
                        if (deviceUtils.readContainerFromDataStore(LogicalDatastoreType.CONFIGURATION,
                            currentPmEntryElem.getPmResourceInstance()) == null) {
                            LOG.warn(EMPTY_INTERFACE_MSG);
                            output.setStatus(RpcStatus.Failed)
                                .setStatusMessage(EMPTY_INTERFACE_MSG);
                            rpcResult = RpcResultBuilder
                                .success(output.build()).build();
                            result.set(rpcResult);
                            return rpcResult;
                        }
                        currentPmEntryDS = getCurrentPmEntry(new ArrayList<>(currentPmListDS
                            .getCurrentPmEntry().values()), currentPmEntryElem);
                        if (currentPmEntryDS == null) {
                            LOG.warn("CurrentPmEntry {} doesn't exist", currentPmEntryElem);
                            output.setStatus(RpcStatus.Failed)
                                .setStatusMessage("At least one CurrentPmEntry doesn't exist");
                            rpcResult = RpcResultBuilder
                                .success(output.build()).build();
                            result.set(rpcResult);
                            return rpcResult;
                        }
                        for (CurrentPm currentPmElem : currentPmEntryElem.getCurrentPm().values()) {
                            CurrentPm currentPmDS = getCurrentPm(new ArrayList<>(currentPmEntryDS
                                .getCurrentPm().values()), currentPmElem);
                            if (currentPmDS == null) {
                                LOG.warn("CurrentPm {} doesn't exist", currentPmElem);
                                output.setStatus(RpcStatus.Failed)
                                    .setStatusMessage("At least one CurrentPm doesn't exist");
                                rpcResult = RpcResultBuilder
                                    .success(output.build()).build();
                                result.set(rpcResult);
                                return rpcResult;
                            }
                            InstanceIdentifier<CurrentPm> iidCurrentPm = iidCurrentPmList
                                .child(CurrentPmEntry.class, new CurrentPmEntryKey(
                                    currentPmEntryElem.getPmResourceInstance(),
                                    currentPmEntryElem.getPmResourceType(),
                                    currentPmEntryElem.getPmResourceTypeExtension()))
                                .child(CurrentPm.class, new CurrentPmKey(
                                    currentPmElem.getDirection(),
                                    currentPmElem.getExtension(),
                                    currentPmElem.getLocation(),
                                    currentPmElem.getType()));
                            if (setCurrentPmMap.containsKey(iidCurrentPm)) {
                                LOG.warn("CurrentPm {} has been already set", currentPmElem);
                                output.setStatus(RpcStatus.Failed)
                                    .setStatusMessage("At least one CurrentPm has been already set");
                                rpcResult = RpcResultBuilder
                                    .success(output.build()).build();
                                result.set(rpcResult);
                                return rpcResult;
                            }
                            tempSetCurrentPmMap.put(iidCurrentPm, currentPmDS);
                        }
                        newCurrentPmEntry.put(new CurrentPmEntryKey(currentPmEntryElem.getPmResourceInstance(),
                            currentPmEntryElem.getPmResourceType(), currentPmEntryElem.getPmResourceTypeExtension()),
                            new CurrentPmEntryBuilder(currentPmEntryElem).build());
                    }
                    // Merge the Maps
                    setCurrentPmMap.putAll(tempSetCurrentPmMap);

                    newCurrentPmList.setCurrentPmEntry(newCurrentPmEntry);
                    deviceUtils.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL,
                        iidCurrentPmList, newCurrentPmList.build(), null);
                    LOG.info("The PMs has been successfully set !");
                    output.setStatus(RpcStatus.Successful)
                        .setStatusMessage("The PMs has been successfully set !");
                    rpcResult = RpcResultBuilder.success(output.build()).build();
                    break;

                case Create:
                    for (org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.pm.interact.input
                            .pm.to.be.set.or.created.CurrentPmEntry currentPmEntryElem : pmInteractInput
                            .getPmToBeSetOrCreated().getCurrentPmEntry().values()) {
                        if (deviceUtils.readContainerFromDataStore(LogicalDatastoreType.CONFIGURATION,
                            currentPmEntryElem.getPmResourceInstance()) == null) {
                            LOG.warn(EMPTY_INTERFACE_MSG);
                            output.setStatus(RpcStatus.Failed)
                                .setStatusMessage(EMPTY_INTERFACE_MSG);
                            rpcResult = RpcResultBuilder.success(output.build()).build();
                            result.set(rpcResult);
                            return rpcResult;
                        }
                        for (CurrentPm currentPmElem : currentPmEntryElem.getCurrentPm().values()) {
                            InstanceIdentifier<CurrentPm> iidCurrentPm = iidCurrentPmList
                                .child(CurrentPmEntry.class, new CurrentPmEntryKey(
                                    currentPmEntryElem.getPmResourceInstance(),
                                    currentPmEntryElem.getPmResourceType(),
                                    currentPmEntryElem.getPmResourceTypeExtension()))
                                .child(CurrentPm.class, new CurrentPmKey(
                                    currentPmElem.getDirection(),
                                    currentPmElem.getExtension(),
                                    currentPmElem.getLocation(),
                                    currentPmElem.getType()));
                            if (setCurrentPmMap.containsKey(iidCurrentPm)) {
                                LOG.warn("CurrentPm {} already exist and is actually set", currentPmElem);
                                output.setStatus(RpcStatus.Failed)
                                    .setStatusMessage("At least one CurrentPm already exist and is actually set");
                                rpcResult = RpcResultBuilder.success(output.build()).build();
                                result.set(rpcResult);
                                return rpcResult;
                            }
                        }
                        newCurrentPmEntry.put(new CurrentPmEntryKey(iidCurrentPmList,
                            currentPmEntryElem.getPmResourceType(), null),
                            new CurrentPmEntryBuilder(currentPmEntryElem).build());
                    }
                    deviceUtils.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL,
                        iidCurrentPmList, newCurrentPmList.setCurrentPmEntry(newCurrentPmEntry).build(), null);
                    LOG.info("The PMs has been successfully created !");
                    output.setStatusMessage("The PMs has been successfully created !");
                    break;

                case Delete:
                    List<InstanceIdentifier<CurrentPmEntry>> iidCurrentPmEntryList = new ArrayList<>();
                    currentPmListDS = deviceUtils.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL,
                        iidCurrentPmList);
                    if (currentPmListDS == null) {
                        LOG.warn(EMPTY_CURRENT_PM_LIST_MSG);
                        output.setStatus(RpcStatus.Failed)
                            .setStatusMessage(EMPTY_CURRENT_PM_LIST_MSG);
                        rpcResult = RpcResultBuilder.success(output.build()).build();
                        result.set(rpcResult);
                        return rpcResult;
                    }
                    for (org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.pm.interact.input
                            .pm.to.get.clear.or.delete.CurrentPmEntry currentPmEntryElem : pmInteractInput
                            .getPmToGetClearOrDelete().getCurrentPmEntry().values()) {
                        // Check that input currentPmEntry exist in the datastore
                        currentPmEntryDS = getCurrentPmEntry(new ArrayList<>(currentPmListDS.getCurrentPmEntry()
                            .values()), currentPmEntryElem);
                        if (currentPmEntryDS == null) {
                            LOG.warn("CurrentPmEntry {} doesn't exist", currentPmEntryElem);
                            output.setStatus(RpcStatus.Failed)
                                .setStatusMessage("At least one CurrentPmEntry doesn't exist");
                            rpcResult = RpcResultBuilder.success(output.build()).build();
                            result.set(rpcResult);
                            return rpcResult;
                        }
                        // Check if the input requests to delete specific CurrentPm
                        if (currentPmEntryElem.getCurrentPm() != null) {
                            for (org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.pm.interact
                                    .input.pm.to.get.clear.or.delete.current.pm.entry
                                    .CurrentPm currentPmElem : currentPmEntryElem.getCurrentPm().values()) {
                                // Check that the CurrentPm exist in the datastore
                                if (!checkCurrentPmDoesExist(new ArrayList<>(currentPmEntryDS.getCurrentPm().values()),
                                        currentPmElem)) {
                                    LOG.warn("CurrentPm {} doesn't exist", currentPmElem);
                                    output.setStatus(RpcStatus.Failed)
                                        .setStatusMessage("At least one CurrentPm doesn't exist");
                                    rpcResult = RpcResultBuilder.success(output.build()).build();
                                    result.set(rpcResult);
                                    return rpcResult;
                                }
                                iidCurrentPmArray.add(iidCurrentPmList
                                    .child(CurrentPmEntry.class, new CurrentPmEntryKey(
                                        currentPmEntryElem.getPmResourceInstance(),
                                        currentPmEntryElem.getPmResourceType(),
                                        currentPmEntryElem.getPmResourceTypeExtension()))
                                    .child(CurrentPm.class, new CurrentPmKey(
                                        currentPmElem.getDirection(),
                                        currentPmElem.getExtension(),
                                        currentPmElem.getLocation(),
                                        currentPmElem.getType())));
                            }
                        }
                        // Check if we have to delete CurrentPmEntry
                        if (currentPmEntryElem.getCurrentPm() == null || currentPmEntryElem.getCurrentPm().isEmpty() ||
                            currentPmEntryElem.getCurrentPm().size() == currentPmEntryDS.getCurrentPm().size())
                            iidCurrentPmEntryList.add(iidCurrentPmList
                                .child(CurrentPmEntry.class, new CurrentPmEntryKey(
                                    currentPmEntryElem.getPmResourceInstance(),
                                    currentPmEntryElem.getPmResourceType(),
                                    currentPmEntryElem.getPmResourceTypeExtension())));
                    }
                    for (InstanceIdentifier<CurrentPm> iidCurrentPm : iidCurrentPmArray) {
                        deviceUtils.deleteContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, iidCurrentPm);
                        setCurrentPmMap.remove(iidCurrentPm);
                    }
                    for (InstanceIdentifier<CurrentPmEntry> iidCurrentPmEntry : iidCurrentPmEntryList)
                        deviceUtils.deleteContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, iidCurrentPmEntry);
                    LOG.info("The PMs has been successfully deleted !");
                    output.setStatusMessage("The PMs has been successfully deleted !");
                    break;

                case Clear:
                    currentPmListDS = deviceUtils.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL,
                        iidCurrentPmList);
                    if (currentPmListDS == null) {
                        LOG.warn(EMPTY_CURRENT_PM_LIST_MSG);
                        output.setStatus(RpcStatus.Failed)
                            .setStatusMessage(EMPTY_CURRENT_PM_LIST_MSG);
                        rpcResult = RpcResultBuilder.success(output.build()).build();
                        result.set(rpcResult);
                        return rpcResult;
                    }
                    for (org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.pm.interact.input
                            .pm.to.get.clear.or.delete.CurrentPmEntry currentPmEntryElem : pmInteractInput
                                .getPmToGetClearOrDelete().getCurrentPmEntry().values()) {
                        currentPmEntryDS = getCurrentPmEntry(new ArrayList<>(currentPmListDS.getCurrentPmEntry()
                            .values()), currentPmEntryElem);
                        if (currentPmEntryDS == null) {
                            LOG.warn("CurrentPmEntry {} doesn't exist", currentPmEntryElem);
                            output.setStatus(RpcStatus.Failed)
                                .setStatusMessage("At least one CurrentPmEntry doesn't exist");
                            rpcResult = RpcResultBuilder.success(output.build()).build();
                            result.set(rpcResult);
                            return rpcResult;
                        }
                        for (org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.pm.interact
                                .input.pm.to.get.clear.or.delete.current.pm.entry
                                .CurrentPm currentPmElem : currentPmEntryElem
                            .getCurrentPm().values()) {
                            InstanceIdentifier<CurrentPm> iidCurrentPm = iidCurrentPmList
                                .child(CurrentPmEntry.class, new CurrentPmEntryKey(
                                    currentPmEntryElem.getPmResourceInstance(),
                                    currentPmEntryElem.getPmResourceType(),
                                    currentPmEntryElem.getPmResourceTypeExtension()))
                                .child(CurrentPm.class, new CurrentPmKey(
                                    currentPmElem.getDirection(),
                                    currentPmElem.getExtension(),
                                    currentPmElem.getLocation(),
                                    currentPmElem.getType()));
                            if (!checkCurrentPmDoesExist(new ArrayList<>(currentPmEntryDS.getCurrentPm().values()),
                                    currentPmElem) || !setCurrentPmMap.containsKey(iidCurrentPm)) {
                                LOG.warn("CurrentPm {} doesn't exist or has never been set", currentPmElem);
                                output.setStatus(RpcStatus.Failed)
                                    .setStatusMessage("At least one CurrentPm doesn't exist or " +
                                        "has never been set");
                                rpcResult = RpcResultBuilder.success(output.build()).build();
                                result.set(rpcResult);
                                return rpcResult;
                            }
                            iidCurrentPmArray.add(iidCurrentPm);
                        }
                    }
                    for (InstanceIdentifier<CurrentPm> iidCurrentPm : iidCurrentPmArray) {
                        deviceUtils.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, iidCurrentPm,
                            setCurrentPmMap.get(iidCurrentPm), null);
                        setCurrentPmMap.remove(iidCurrentPm);
                    }
                    LOG.info("The PMs has been successfully released !");
                    output.setStatusMessage("The PMs has been successfully released !");
                    break;
                }
                output.setStatus(RpcStatus.Successful);
               rpcResult = RpcResultBuilder
                    .success(output.build()).build();
                result.set(rpcResult);
                return rpcResult;
            }
        });
        return result;
    }

    private boolean validateInput(PmInteractInput pmInteractInput, PmInteractOutputBuilder output) {
        if (pmInteractInput == null || pmInteractInput.getRpcAction() == null) {
            LOG.warn("The RPC action input is null");
            output.setStatus(RpcStatus.Failed)
                .setStatusMessage("The RPC action input is null");
            return false;
        }
        HoneynodeRpcActions rpcAction = pmInteractInput.getRpcAction();
        switch (rpcAction) {
        case Get:
            if (pmInteractInput.getPmToGetClearOrDelete() != null &&
                pmInteractInput.getPmToGetClearOrDelete().getCurrentPmEntry() != null)
                for (org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.pm.interact
                    .input.pm.to.get.clear.or.delete.CurrentPmEntry currentPmEntry : pmInteractInput
                    .getPmToGetClearOrDelete().getCurrentPmEntry().values()) {
                    if (currentPmEntry.getPmResourceInstance() == null ||
                        currentPmEntry.getPmResourceType() == null ||
                        currentPmEntry.getPmResourceTypeExtension() == null) {
                        LOG.warn(INVALID_CURRENT_PM_ENTRY_MSG);
                        output.setStatus(RpcStatus.Failed)
                            .setStatusMessage(INVALID_CURRENT_PM_ENTRY_MSG);
                        return false;
                    }
                }
            break;

        case Delete:
        case Clear:
            PmToGetClearOrDelete pmToGetClearOrDelete = pmInteractInput.getPmToGetClearOrDelete();
            if (pmToGetClearOrDelete == null || pmToGetClearOrDelete.getCurrentPmEntry() == null) {
                LOG.warn(INVALID_PM_GET_CLEAR_OR_DELETE);
                output.setStatus(RpcStatus.Failed)
                    .setStatusMessage(INVALID_PM_GET_CLEAR_OR_DELETE);
                return false;
            }
            for (org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.pm.interact
                    .input.pm.to.get.clear.or.delete.CurrentPmEntry currentPmEntry : pmToGetClearOrDelete
                    .getCurrentPmEntry().values()) {
                if (currentPmEntry.getPmResourceInstance() == null ||
                    currentPmEntry.getPmResourceType() == null ||
                    currentPmEntry.getPmResourceTypeExtension() == null ||
                    (rpcAction == HoneynodeRpcActions.Clear &&
                        (currentPmEntry.getCurrentPm() == null || currentPmEntry.getCurrentPm().isEmpty()))) {
                    LOG.warn(INVALID_CURRENT_PM_ENTRY_MSG);
                    output.setStatus(RpcStatus.Failed)
                        .setStatusMessage(INVALID_CURRENT_PM_ENTRY_MSG);
                    return false;
                }
                if (currentPmEntry.getCurrentPm() != null) {
                    for (org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.pm.interact
                            .input.pm.to.get.clear.or.delete.current.pm.entry.CurrentPm currentPm : currentPmEntry
                            .getCurrentPm().values()) {
                        if (currentPm.getDirection() == null ||
                            currentPm.getLocation() == null ||
                            currentPm.getExtension() == null ||
                            currentPm.getType() == null) {
                            LOG.warn(INVALID_CURRENT_PM_MSG);
                            output.setStatus(RpcStatus.Failed)
                                .setStatusMessage(INVALID_CURRENT_PM_MSG);
                            return false;
                        }
                    }
                }
            }
            break;

        case Set:
        case Create:
            PmToBeSetOrCreated pmToBeSetOrCreated = pmInteractInput.getPmToBeSetOrCreated();
            if (pmToBeSetOrCreated == null ||
                pmToBeSetOrCreated.getCurrentPmEntry() == null ||
                pmToBeSetOrCreated.getCurrentPmEntry().isEmpty()) {
                LOG.warn(INVALID_PM_BE_SET_OR_CREATED);
                output.setStatus(RpcStatus.Failed)
                    .setStatusMessage(INVALID_PM_BE_SET_OR_CREATED);
                return false;
            }
            for (org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.pm.interact
                    .input.pm.to.be.set.or.created.CurrentPmEntry currentPmEntry : pmToBeSetOrCreated
                    .getCurrentPmEntry().values()) {
                if (currentPmEntry.getCurrentPm() == null ||
                    currentPmEntry.getPmResourceInstance() == null ||
                    currentPmEntry.getPmResourceType() == null ||
                    currentPmEntry.getPmResourceTypeExtension() == null) {
                    LOG.warn(INVALID_CURRENT_PM_ENTRY_MSG);
                    output.setStatus(RpcStatus.Failed)
                        .setStatusMessage(INVALID_CURRENT_PM_ENTRY_MSG);
                    return false;
                }
                for (CurrentPm currentPm : currentPmEntry.getCurrentPm().values()) {
                    if (currentPm.getDirection() == null ||
                        currentPm.getLocation() == null ||
                        currentPm.getExtension() == null ||
                        currentPm.getType() == null ||
                        currentPm.getMeasurement() == null) {
                        LOG.warn(INVALID_CURRENT_PM_MSG);
                        output.setStatus(RpcStatus.Failed)
                            .setStatusMessage(INVALID_CURRENT_PM_MSG);
                        return false;
                    }
                    for (Measurement measurement : currentPm.getMeasurement().values()) {
                        if (measurement.getPmParameterValue() == null ||
                            measurement.getGranularity() == null) {
                            LOG.warn(INVALID_MEASUREMENT);
                            output.setStatus(RpcStatus.Failed)
                                .setStatusMessage(INVALID_MEASUREMENT);
                            return false;
                        }
                    }
                }
            }
            break;

        default:
            output.setStatus(RpcStatus.Failed).setStatusMessage("Unexpected RPC-Action");
            return false;
        }
        return true;
    }

    private Boolean checkCurrentPmDoesExist(List<CurrentPm> currentPmList,
        org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.pm.interact
            .input.pm.to.get.clear.or.delete.current.pm.entry.CurrentPm currentPmToCheck) {
        return currentPmList.stream().anyMatch(currentPm -> currentPm.getType().equals(currentPmToCheck.getType()) &&
            currentPm.getExtension().equals(currentPmToCheck.getExtension()) &&
            currentPm.getLocation().equals(currentPmToCheck.getLocation()) &&
            currentPm.getDirection().equals(currentPmToCheck.getDirection()));
    }

    private CurrentPmEntry getCurrentPmEntry(List<CurrentPmEntry> currentPmEntryList,
        org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.pm.interact
            .input.pm.to.get.clear.or.delete.CurrentPmEntry currentPmEntryToGet) {
        return currentPmEntryList.stream()
            .filter(currentPmEntryElem -> currentPmEntryElem.getPmResourceInstance()
                .equals(currentPmEntryToGet.getPmResourceInstance())
                && currentPmEntryElem.getPmResourceType()
                    .equals(currentPmEntryToGet.getPmResourceType())
                && currentPmEntryElem
                    .getPmResourceTypeExtension().equals(currentPmEntryToGet.getPmResourceTypeExtension()))
            .findFirst()
            .orElse(null);
    }

    private CurrentPmEntry getCurrentPmEntry(List<CurrentPmEntry> currentPmEntryList,
        org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev230629.pm.interact
            .input.pm.to.be.set.or.created.CurrentPmEntry currentPmEntryToGet) {
        return currentPmEntryList.stream()
            .filter(currentPmEntryElem -> currentPmEntryElem.getPmResourceInstance()
                .equals(currentPmEntryToGet.getPmResourceInstance())
                && currentPmEntryElem.getPmResourceType()
                    .equals(currentPmEntryToGet.getPmResourceType())
                && currentPmEntryElem
                    .getPmResourceTypeExtension().equals(currentPmEntryToGet.getPmResourceTypeExtension()))
            .findFirst()
            .orElse(null);
    }

    private CurrentPm getCurrentPm(List<CurrentPm> currentPmEntryList, CurrentPm currentPmToGet) {
        return currentPmEntryList.stream()
            .filter(currentPmElem -> currentPmElem.getType()
                .equals(currentPmToGet.getType())
                && currentPmElem.getExtension()
                    .equals(currentPmToGet.getExtension())
                && currentPmElem
                    .getLocation().equals(currentPmToGet.getLocation())
                &&
                currentPmElem.getDirection().equals(currentPmToGet.getDirection()))
            .findFirst()
            .orElse(null);
    }

    @Override
    public void close() throws Exception {
        this.executor.shutdown();
    }
}
