package io.lighty.transportpce.netconf.device.listener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.opendaylight.mdsal.binding.api.DataObjectModification;
import org.opendaylight.mdsal.binding.api.DataTreeChangeListener;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.mdsal.binding.api.DataTreeModification;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021.CurrentPmDescription;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021.pm.description.PmGenerationRules;
import org.opendaylight.yang.gen.v1.http.org.openroadm.common.types.rev181019.State;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.ChangeNotification;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.ChangeNotification.Datastore;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.ChangeNotificationBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.OrgOpenroadmDeviceData;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.change.notification.Edit;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.change.notification.EditBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.changed.by.parms.ChangedBy;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.changed.by.parms.ChangedByBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.changed.by.parms.changed.by.server.or.user.ServerBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.circuit.pack.Ports;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.circuit.pack.PortsBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.circuit.pack.PortsKey;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.circuit.packs.CircuitPacks;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.circuit.packs.CircuitPacksKey;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.interfaces.grp.Interface;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.org.openroadm.device.container.OrgOpenroadmDevice;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev181019.current.pm.group.CurrentPm;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev181019.current.pm.list.CurrentPmEntry;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.types.rev171215.PmDataType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.types.rev171215.PmGranularity;
import org.opendaylight.yangtools.yang.binding.DataObjectStep;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.opendaylight.yangtools.yang.common.Decimal64;
import org.opendaylight.yangtools.yang.common.Empty;
import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.netconf.base._1._0.rev110601.EditOperationType;
import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.yang.ietf.yang.types.rev130715.DateAndTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import io.lighty.transportpce.netconf.device.openroadm.DeviceUtils;
import io.lighty.transportpce.netconf.device.requests.notification.NotificationPublishService;

public class CurrentPmChangeListener221 implements DataTreeChangeListener<CurrentPm> {

    private static final Logger LOG = LoggerFactory.getLogger(CurrentPmChangeListener221.class);
    private DeviceUtils deviceUtils;
    private NotificationPublishService notificationPublishService;

    public CurrentPmChangeListener221(DeviceUtils deviceUtils) {
        this.deviceUtils = deviceUtils;
    }

    @Override
    public void onDataTreeChanged(List<DataTreeModification<CurrentPm>> changes) {
        LOG.info("onDataTreeChanged - CurrentPm");
        for (DataTreeModification<CurrentPm> change : changes) {
            final DataObjectModification<CurrentPm> rootNode = change.getRootNode();
            final DataTreeIdentifier<CurrentPm> rootPath = change.getRootPath();
            switch (rootNode.modificationType()) {
            case SUBTREE_MODIFIED:
            case WRITE:
                CurrentPmDescription currentPmDescription = deviceUtils
                    .readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL,
                        InstanceIdentifier.create(CurrentPmDescription.class));
                if (rootNode.dataAfter().getMeasurement() != null &&
                    !rootNode.dataAfter().getMeasurement().isEmpty() &&
                    currentPmDescription != null) {
                    LinkedList<DataObjectStep<?>> path = new LinkedList<>();
                    rootPath.path().getPathArguments().forEach(p -> path.add(p));
                    path.removeLast();
                    InstanceIdentifier<CurrentPmEntry> iidCurrentPmEntry = InstanceIdentifier.unsafeOf(path);
                if ((Interface) deviceUtils.readContainerFromDataStore(
                        LogicalDatastoreType.CONFIGURATION,
                        InstanceIdentifier.keyOf(iidCurrentPmEntry).getPmResourceInstance()) == null) {
                    break;
                }
                Interface theInterface = (Interface) deviceUtils.readContainerFromDataStore(
                        LogicalDatastoreType.CONFIGURATION,
                        InstanceIdentifier.keyOf(iidCurrentPmEntry).getPmResourceInstance());
                    // Get the PmGenerationRules that may administer the CurrentPm
                    PmGenerationRules targetPmGenerationRules = currentPmDescription.getPmGenerationRules().values()
                        .stream()
                        .filter(pmGenerationRules -> theInterface.getType()
                            .equals(pmGenerationRules.getTriggeringInterface()) &&
                            checkIfSamePmType(rootNode.dataAfter(), pmGenerationRules))
                        .findFirst()
                        .orElse(null);
                    if (targetPmGenerationRules == null) {
                        continue;
                    }
                    InstanceIdentifier<Ports> iiPort = InstanceIdentifier
                        .builderOfInherited(OrgOpenroadmDeviceData.class, OrgOpenroadmDevice.class)
                        .child(CircuitPacks.class, new CircuitPacksKey(theInterface.getSupportingCircuitPackName()))
                        .child(Ports.class, new PortsKey((String) theInterface.getSupportingPort())).build();
                    Ports port = deviceUtils.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, iiPort);
                    // Check if the measurement is out of the boundaries defined by the PmGenerationRules
                    boolean havePmOutOfBounds = rootNode.dataAfter().getMeasurement().values().stream()
                        .filter(measure -> measure.getGranularity().equals(PmGranularity._15min))
                        .anyMatch(pmValue -> checkIfPmValueOutOfBounds(pmValue.getPmParameterValue(),
                                targetPmGenerationRules));
                    if (havePmOutOfBounds && port.getOperationalState().equals(State.InService)) {
                        LOG.warn("Detected abnormal {} PM parameter value !", rootNode.dataAfter().getType());
                        changeOperationalStatePort(port, iiPort, State.OutOfService);
                    } else if (port.getOperationalState().equals(State.OutOfService)
                            && !havePmOutOfBounds
                            && rootNode.dataBefore() != null
                            && !rootNode.dataBefore().getMeasurement().isEmpty()
                            && rootNode.dataBefore().getMeasurement().values().stream()
                            .anyMatch(pmValue -> checkIfPmValueOutOfBounds(pmValue.getPmParameterValue(),
                                    targetPmGenerationRules))) {
                        changeOperationalStatePort(port, iiPort, State.InService);
                    }
                }
                break;
            default:
                break;
            }

        }
    }

    private void changeOperationalStatePort(Ports port, InstanceIdentifier<Ports> iiPort, State newState) {
        ChangeNotification changeNotification = createNotification(iiPort);
        deviceUtils.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, iiPort,
            new PortsBuilder(port).setOperationalState(newState).build(), changeNotification);
    }

    private ChangeNotification createNotification(InstanceIdentifier<Ports> id) {
        List<Edit> editList = new ArrayList<>();
        Edit edit = new EditBuilder()
                .setOperation(EditOperationType.Merge)
                .setTarget(id)
                .build();
        editList.add(edit);
        ChangedBy changedBy = new ChangedByBuilder().setServerOrUser(new ServerBuilder().setServer(Empty.value())
            .build())
            .build();
        String time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX").format(new Date());
        return new ChangeNotificationBuilder().setChangeTime(new DateAndTime(time))
            .setDatastore(Datastore.Running).setEdit(editList).build();
    }

    private boolean checkIfPmValueOutOfBounds(PmDataType pmDataType, PmGenerationRules pmGenerationRules) {
        Decimal64 pmValue = Decimal64.valueOf(new BigDecimal(pmDataType.stringValue()));
        return pmValue.compareTo(Decimal64.valueOf(new BigDecimal(pmGenerationRules.getMaxValue().toString()))) > 0 ||
                pmValue.compareTo(Decimal64.valueOf(pmGenerationRules.getMinValue().toString())) < 0;
    }

    private boolean checkIfSamePmType(CurrentPm currentPm, PmGenerationRules pmGenerationRules) {
        return currentPm.getType().getName().equalsIgnoreCase(pmGenerationRules.getPmNamePrefix());
    }

}
