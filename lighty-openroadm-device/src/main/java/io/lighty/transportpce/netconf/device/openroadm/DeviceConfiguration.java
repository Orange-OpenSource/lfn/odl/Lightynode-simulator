package io.lighty.transportpce.netconf.device.openroadm;

import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public final class DeviceConfiguration {
    private static final Logger LOG = LoggerFactory.getLogger(DeviceConfiguration.class);

    public DeviceConfiguration() {
    }


    public static String loadConfigData(String configXSL) {
        LOG.info("loading device configuration info from xml file...");
        String config_result = null;
        String xml = operToConfig();
        if (xml != null) {
            LOG.info("process to transform xml file to config data");
            TransformerFactory factory = TransformerFactory.newInstance();
            Source xslt = new StreamSource(DeviceConfiguration.class.getResourceAsStream(configXSL));
            Transformer transformer;
            Source text;
            StringWriter device_config = new StringWriter();
            try {
                LOG.info("transforming xml string to config device ...");
                transformer = factory.newTransformer(xslt);
                text = new StreamSource(new StringReader(xml));
                transformer.transform(text, new StreamResult(device_config));
                config_result = device_config.toString();
            } catch (TransformerException e) {
                LOG.error("Transformer failed ");
            }
        }
        return config_result;
    }

    private static String operToConfig() {
        String result = null;
        LOG.info("process to transform xml file {}", DeviceUtils.DEVICE_DATA_SAMPLE_OPER_XML);
        TransformerFactory factory = TransformerFactory.newInstance();
        Source xslt = new StreamSource(DeviceConfiguration.class.
            getResourceAsStream(DeviceUtils.DEVICE_XSL));
        LOG.info("source, {}", xslt.getSystemId());
        StringWriter tmpWriter = new StringWriter();
        try {
            LOG.info("transforming xml data to config device ...");
            Transformer transformer = factory.newTransformer(xslt);
            String extract_data = ExtractXMLTag.extractTagElement(DeviceUtils.DEVICE_DATA_SAMPLE_OPER_XML,
                "org-openroadm-device", "http://org/openroadm/device");
            StreamSource text = new StreamSource(new StringReader(extract_data));
            LOG.info("text avant transform = {}", text.toString());
            transformer.transform(text, new StreamResult(tmpWriter));
            result = tmpWriter.toString();
        }
        catch (TransformerException e) {
            LOG.error("Transformer failed ", e);
        }
        return  result;
    }
}
