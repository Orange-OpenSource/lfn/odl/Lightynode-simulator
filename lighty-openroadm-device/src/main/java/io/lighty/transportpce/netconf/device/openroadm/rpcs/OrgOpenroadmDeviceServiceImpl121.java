/*
 * Copyright (c) 2020 PANTHEON.tech s.r.o. All Rights Reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-v10.html
 */
package io.lighty.transportpce.netconf.device.openroadm.rpcs;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;

import io.lighty.transportpce.netconf.device.openroadm.DeviceUtils;
import io.lighty.transportpce.netconf.device.requests.notification.NotificationPublishService;

import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.opendaylight.yang.gen.v1.http.org.openroadm.common.types.rev161014.RpcStatus;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.GetConnectionPortTrailInput;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.GetConnectionPortTrailOutput;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.GetConnectionPortTrailOutputBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.get.connection.port.trail.output.Ports;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev170206.get.connection.port.trail.output.PortsBuilder;
import org.opendaylight.yangtools.yang.common.RpcResult;
import org.opendaylight.yangtools.yang.common.RpcResultBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrgOpenroadmDeviceServiceImpl121 implements AutoCloseable {

    private static final Logger LOG = LoggerFactory.getLogger(OrgOpenroadmDeviceServiceImpl121.class);

    private final ExecutorService executor;
    private NotificationPublishService notificationPublishService;

    public NotificationPublishService getNotificationPublishService() {
        return notificationPublishService;
    }

    public void setNotificationPublishService(NotificationPublishService notificationPublishService) {
        this.notificationPublishService = notificationPublishService;
    }

    public OrgOpenroadmDeviceServiceImpl121() {
        this.executor = Executors.newFixedThreadPool(1);
    }

    public void setDataBrokerService(final DeviceUtils deviceUtils) {
    }
    
    public ListenableFuture<RpcResult<GetConnectionPortTrailOutput>> getConnectionPortTrail(
            GetConnectionPortTrailInput input) {
        LOG.info("RPC GetConnectionPortTrail request received !");
        final SettableFuture<RpcResult<GetConnectionPortTrailOutput>> result = SettableFuture.create();
        this.executor.submit(new Callable<RpcResult<GetConnectionPortTrailOutput>>() {
            Ports port = new PortsBuilder()
                .setCircuitPackName("2/0")
                .setPortName("L1")
                .build();
            @Override
            public RpcResult<GetConnectionPortTrailOutput> call() throws Exception {
                final GetConnectionPortTrailOutput output = new GetConnectionPortTrailOutputBuilder()
                    .setPorts(Arrays.asList(port))
                    .setStatusMessage("OK")
                    .setStatus(RpcStatus.Successful)
                    .build();
                final RpcResult<GetConnectionPortTrailOutput> rpcResult = RpcResultBuilder.success(output).build();
                result.set(rpcResult);
                return rpcResult;
            }
        });
        return result;
    }

    @Override
    public void close() throws Exception {
        this.executor.shutdown();
    }
}
