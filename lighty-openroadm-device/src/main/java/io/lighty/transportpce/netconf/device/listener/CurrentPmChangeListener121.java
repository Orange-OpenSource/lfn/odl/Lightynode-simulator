package io.lighty.transportpce.netconf.device.listener;

import java.util.List;

import org.opendaylight.mdsal.binding.api.DataTreeChangeListener;
import org.opendaylight.mdsal.binding.api.DataTreeModification;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev161014.currentpmlist.CurrentPm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.lighty.transportpce.netconf.device.openroadm.DeviceUtils;
import io.lighty.transportpce.netconf.device.requests.notification.NotificationPublishService;

public class CurrentPmChangeListener121 implements DataTreeChangeListener<CurrentPm> {

    private static final Logger LOG = LoggerFactory.getLogger(CurrentPmChangeListener121.class);
    private DeviceUtils deviceUtils;
    private NotificationPublishService notificationPublishService;

    public void setNotificationPublishService(NotificationPublishService notificationPublishService) {
        this.notificationPublishService = notificationPublishService;
        LOG.info("dans setNotificationPublishService");
        LOG.info(this.notificationPublishService.toString());
    }

    public CurrentPmChangeListener121(DeviceUtils deviceUtils) {
        this.deviceUtils = deviceUtils;
    }

    @Override
    public void onDataTreeChanged(List<DataTreeModification<CurrentPm>> changes) {
        LOG.info("onDataTreeChanged - CurrentPm");
    }

}
