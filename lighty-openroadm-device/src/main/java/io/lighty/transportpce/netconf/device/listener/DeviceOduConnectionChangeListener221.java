package io.lighty.transportpce.netconf.device.listener;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.opendaylight.mdsal.binding.api.DataObjectModification;
import org.opendaylight.mdsal.binding.api.DataTreeChangeListener;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.mdsal.binding.api.DataTreeModification;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev181019.org.openroadm.device.container.org.openroadm.device.OduConnection;
import org.opendaylight.yangtools.yang.binding.DataObjectStep;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lighty.transportpce.netconf.device.openroadm.DeviceUtils;

/**
 * The listener interface for receiving deviceOduConnectionChange events.
 * The class that is interested in processing a deviceOduConnectionChange
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's addDeviceOduConnectionChangeListener method. When
 * the deviceOduConnectionChange event occurs, that object's appropriate
 * method is invoked.
 */
public class DeviceOduConnectionChangeListener221 implements DataTreeChangeListener<OduConnection> {
    private static final Logger LOG = LoggerFactory.getLogger(DeviceOduConnectionChangeListener221.class);
    private DeviceUtils deviceUtils;

    public DeviceOduConnectionChangeListener221(DeviceUtils deviceUtils) {
        this.deviceUtils = deviceUtils;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onDataTreeChanged(@NonNull List<DataTreeModification<OduConnection>> changes) {
        LOG.info("onDataTreeChanged - OduConnection");
        for (DataTreeModification<OduConnection> change : changes) {
            final DataObjectModification<OduConnection> rootNode = change.getRootNode();
            final DataTreeIdentifier<OduConnection> rootPath = change.getRootPath();
            LOG.debug("Received OduConnection change({}):\n dataType={} \n before={} \n after={} \n identifier = {}",
                rootNode.modificationType(), rootNode.dataType(), rootNode.dataBefore(),
                rootNode.dataAfter(), rootNode.step());
            InstanceIdentifier<OduConnection> connectionId = null;
            if (rootPath.path().getTargetType().getName().equals(OduConnection.class.getName())) {
                LinkedList<DataObjectStep<?>> path = new LinkedList<>();
                rootPath.path().getPathArguments().forEach(p -> path.add(p));
                connectionId = InstanceIdentifier.unsafeOf(path);
            }
            switch (rootNode.modificationType()) {
            case SUBTREE_MODIFIED:
            case WRITE:
                LOG.info("OduConnection is being created or modified");
                copyConnectionOnOperationalDS(rootNode.dataAfter(), connectionId);
                break;
            case DELETE:
                LOG.info("OduConnection is being deleted");
                deleteOduConnectionFromDataStore(LogicalDatastoreType.OPERATIONAL, connectionId);
                break;
            default:
                break;
            }
        }
    }

    private void deleteOduConnectionFromDataStore(LogicalDatastoreType datastore,
        InstanceIdentifier<OduConnection> connectionId) {
        LOG.info("deleting container {} from {} datastore ...", connectionId.getTargetType().getSimpleName(),
            datastore);
        deviceUtils.deleteContainerFromDataStore(datastore, connectionId);
    }

    private void copyConnectionOnOperationalDS(OduConnection connectionAfter,
        InstanceIdentifier<OduConnection> connectionId) {
        if (connectionId == null) {
            LOG.warn("Error to copy connection in operational DS - connection instance id is null");
            return;
        }
        OduConnection oduConnection = deviceUtils.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL,
                connectionId);
        if (oduConnection != null) {
            LOG.warn("Error creating connection {} - it already exists", connectionAfter.getConnectionName());
            return;
        }
        LOG.info("creating new connection {} in operational DS", connectionAfter.getConnectionName());
        deviceUtils.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, connectionId, connectionAfter, null);
    }
}
